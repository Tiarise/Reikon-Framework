#include "Core/Reikon.h"
#include "Core/FrameworkTypes.h"
#include <iostream>
//#include "Dependencies/CatchUnitTest/catch_amalgamated.hpp"

void ExampleUsageOne()
{
	//Create entity
	ReikonCore::Reikon creatureOne("#LOC_CreatureName_LordFungusTheFirst", "#LOC_Faction_Human");

	//Create attribute for creatureOne ( Option one - Create in place )
	ReikonCore::Attribute* health = creatureOne.AttributeHandler()->AddNewAttribute("#LOC_Attribute_Health",
		ReikonCore::AttributeType::MaxHealth, ReikonCore::AttributeGroup::Vital, 100);

	//Create attribute for creatureOne ( Option two - Pass an existing attribute)
	ReikonCore::Attribute strengthLocal("#LOC_Attribute_Strength",ReikonCore::AttributeType::Strength,
		ReikonCore::AttributeGroup::MainAttribute, 10);
	ReikonCore::Attribute* strength = creatureOne.AttributeHandler()->AddNewAttribute(strengthLocal);

	if ( !health || !strength )
		return;

	//Strength should modify health by 2 times of it's value.
	health->AddModification(strength,2.0f);

	//Create a new item.
	ReikonCore::Item newItem("#LOC_Item_FancyNecklace",ReikonCore::ItemSlotType::Necklace);

	//Add modifier to item.
	ReikonCore::ItemModifier itemModifier = ReikonCore::ItemModifier(ReikonCore::AttributeType::Strength,false,10);
	newItem.AddModification(itemModifier);

	//Equip item on creatureOne.
	ReikonCore::InventorySlot* slot = creatureOne.Inventory()->AddToInventory(newItem);
	creatureOne.Inventory()->EquipItem(slot,ReikonCore::ItemSlotType::Necklace);
    
    std::cout << "<*> ExampleUsageOne function successfully finished!" << std::endl;
}

//void UnitTestingFromMain(int argc, char** argv)
//{
//	Catch::Session().run( argc, argv );
//}

int main(int argc, char** argv)
{
	ExampleUsageOne();
	//UnitTestingFromMain(argc,argv);
    return 0;
}