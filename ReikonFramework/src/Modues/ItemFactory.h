﻿#pragma once
#include <string>
#include "../Core/FrameworkTypes.h"
#include "../Core/Item.h"
#include "../Dependencies/JsonCpp/json.h"

namespace ReikonModules
{
    //Helper struct for item building.
    struct ItemInfo
    {
        ItemInfo(std::string baseItemName,std::string modifierName,
            int levelRequirement,int lowerBoundary, int upperBoundary, ReikonCore::ItemSlotType type ) : 
            m_BaseItemName(std::move(baseItemName)), m_ModifierName(std::move(modifierName)),
            m_LevelRequirement(levelRequirement), m_LowerBoundary(lowerBoundary), m_UpperBoundary(upperBoundary),
            m_ItemType(type)  {}

        std::string m_BaseItemName;
        std::string m_ModifierName;
        unsigned int m_LevelRequirement;
        unsigned int m_LowerBoundary;
        unsigned int m_UpperBoundary; 
        ReikonCore::ItemSlotType m_ItemType;       
    };

    /*
        Functions are in the class in suggested usage order.

        JSON:
        --------------------------------------------------
        LevelRequirement,Rarity is int
        The smaller the rarity number the rarer the item.
        Currently from items we only accept flat modifications. ( Design )

        IMPORTANT:
        ------------------------------------------------------------------------------
        With Rider ( Or other IDE ) while using .sln make sure that the solution's
        LocalDebugger/WorkingDirectory is $(SolutionDir)$(Platform)\$(Configuration)\
        and the folder containing the json is in it!
    */
    class ItemFactory
    {
    public:
        ItemFactory() = default;
        virtual ~ItemFactory() = default;
        ItemFactory(const ItemFactory& orig) = delete;
        ItemFactory& operator= (ItemFactory const&) = delete;
		ItemFactory(ItemFactory&& orig) = delete;
		ItemFactory& operator=(ItemFactory&& orig) = delete;
        
        //There is no need to have a '/' after templatePrefix.
        bool SetConfigFolder(const std::string& templateFolder);

        bool LoadItemData();
        bool BuildItemListWithRarity();

        ReikonCore::Item* GenerateRandomItem(ReikonCore::ItemSlotType type , unsigned int maxLevel = -1,bool considerRarity=true); 
        ReikonCore::Item* GenerateItem(const std::string& baseItemName,const std::string& modifierName); 
        
    private:
        ReikonCore::ItemSlotType GetItemSlotTypeFromString(const std::string& baseGroupName) const;
        ReikonCore::AttributeType GetAttributeTypeFromString(const std::string& attributeName) const;
        bool CheckLevelRequirement(int baseItemLevel,const std::string& modifierGroup) const;
        std::string ItemStatType(const std::string& itemStat) const;
        std::string ItemStatTarget(const std::string& itemStat) const;
        
        std::vector<ItemInfo> m_ItemRarityList;
        std::string m_TemplateFolder;
        Json::Value m_ItemsTemplate;
    };    
}


