﻿#include "ItemFactory.h"
#include <fstream>
#include <sys/stat.h> //Folder checking.
#include "../Dependencies/SeededRandom.hpp"
#include "../Core/Item.h"
#include <filesystem> //C++17

namespace ReikonModules
{
    const std::string ItemDB = "ItemList.json";
    
    bool ItemFactory::SetConfigFolder(const std::string& templateFolder)
    {
        struct stat info{};
        
        //Stat returns 0 on success and -1 on fail.
        if( stat( templateFolder.c_str(), &info ) != 0 )
            return false;
    
        m_TemplateFolder = templateFolder;
        return true;      
    }

    bool ItemFactory::LoadItemData()
    {
        std::string in;
        if ( !m_TemplateFolder.empty() )
            in = m_TemplateFolder + "/" + ItemDB;
        else
            in = ItemDB; //No folder set, check in place.

        if ( !std::filesystem::exists(in) ) //C++17
            return false;
            
        std::ifstream file(in.c_str(), std::ios::binary | std::ios::ate); 
        std::streamsize size = file.tellg();
        file.seekg(0, std::ios::beg);
        
        std::vector<char> buffer(size);
        if ( size <= 0 || !file.read(buffer.data(), size) )
        {   
            file.close();
            return false; //File not found or invalid.
        }

        //Convert to string
        std::string toParse(buffer.begin(), buffer.end());

        //Start parsing with json reader
        Json::Reader reader;
        bool parsingSuccessful = reader.parse(toParse, m_ItemsTemplate ); 
        if ( !parsingSuccessful )
            return false; //Error during parsing ItemDB

        file.close();
        return true;
    }

    bool ItemFactory::BuildItemListWithRarity()
    {
        //Within the vector there will be [ (leather chest,Layered,1,5), (leather chest,Arcane,6,11)]
        int entryLowerBoundary = 1;
        int entryUpperBoundary = 1;  

        //Iterate trough the item types
        for( Json::ValueIterator itemTypeItr = m_ItemsTemplate.begin() ; itemTypeItr != 
            m_ItemsTemplate.end() ; itemTypeItr++ )
        {
            ReikonCore::ItemSlotType slotType = GetItemSlotTypeFromString(itemTypeItr.key().asString());

            //Iterate trough the base items in the specific item-type group
            for( Json::ValueIterator base_item_it = m_ItemsTemplate[itemTypeItr.key().asString()]["BaseItem"].begin() ; 
                 base_item_it != m_ItemsTemplate[itemTypeItr.key().asString()]["BaseItem"].end() ; base_item_it++ )
            {
                const std::string baseItemName = base_item_it.key().asString();
                const int baseItemLevelRequirement = (*base_item_it)["LevelRequirement"].asInt();
                const int baseItemRarity =  (*base_item_it)["Rarity"].asInt();

                /*
                    We go trough all the modifications in the item type-group.
                    Because we need to check for all available modifications for all base item types separately in
                    the given item-type group. Example : Group-*-1-10 contains several modification type
                */
                for( Json::ValueIterator itemModifGroupItr = m_ItemsTemplate[itemTypeItr.key().asString()]["ModifierGroups"].begin() ; 
                    itemModifGroupItr != m_ItemsTemplate[itemTypeItr.key().asString()]["ModifierGroups"].end() ; itemModifGroupItr++ )
                {
                    /*
                        We get the modifier group level from the modifier group name  (example  Group-*-1-10 )
                        This is also the NAME of the modifier group so it can be used as identifier.
                    */
                    std::string modifierGroupLevel = itemModifGroupItr.key().asString();

                    /*
                        Now we check if the base Item has the level requirement for the item group 
                        if the modifier level is accepted by base item.
                    */
                    if ( !CheckLevelRequirement(baseItemLevelRequirement,modifierGroupLevel) )
                        continue;

                    /*
                        Now we go trough each modification in the given modification group. 
                        Example : "Layered" prefix modification.
                    */
                    for( Json::ValueIterator itemModifItr = m_ItemsTemplate[itemTypeItr.key().asString()]["ModifierGroups"][modifierGroupLevel].begin() ; 
                      itemModifItr != m_ItemsTemplate[itemTypeItr.key().asString()]["ModifierGroups"][modifierGroupLevel].end() ; itemModifItr++ ) 
                    {
                        const int modifierRarity = (*itemModifItr)["Rarity"].asInt();
                        std::string modifierName = itemModifItr.key().asString();
                                  
                        entryUpperBoundary = entryLowerBoundary + (  baseItemRarity*modifierRarity  )-1;
                        this->m_ItemRarityList.emplace_back( ItemInfo(baseItemName, modifierName,
                                    baseItemLevelRequirement,entryLowerBoundary, entryUpperBoundary,slotType) );
                        entryLowerBoundary = entryUpperBoundary+1;
                    }                    
                }
            }
        }
        
        return false;
    }

    ReikonCore::Item* ItemFactory::GenerateRandomItem(ReikonCore::ItemSlotType type, unsigned int maxLevel, bool considerRarity)
    {
        if ( m_ItemRarityList.empty() )
            return nullptr;
        
        int newIndex = 0;
        std::vector<ItemInfo> filteredList ; //We create a new filtered list based on the original if needed.
        std::vector<ItemInfo>::iterator rarityListItr;
        
        //If we need to filter the list for appropriate leveled items.
        if ( maxLevel  != -1 )
        {
            //Generate a filtered list with the available items what pass the lvl filter
            for( rarityListItr = m_ItemRarityList.begin(); rarityListItr != m_ItemRarityList.end(); rarityListItr++ )
            {
                if (  rarityListItr->m_LevelRequirement <= maxLevel && rarityListItr->m_ItemType == type )  
                {
                    ItemInfo  tmp = *rarityListItr;
                    tmp.m_LowerBoundary = newIndex +1;
                    tmp.m_UpperBoundary = rarityListItr->m_UpperBoundary - rarityListItr->m_LowerBoundary;
                    filteredList.push_back(*rarityListItr);  //push_back creates a copy of the object, we use this on purpose. 
                }
            }
        }
        else //Non level filtered list
        {
            for( rarityListItr = m_ItemRarityList.begin(); rarityListItr != m_ItemRarityList.end(); rarityListItr++ )
            {
                if (  rarityListItr->m_ItemType == type )  
                {
                    ItemInfo  tmp = *rarityListItr;
                    tmp.m_LowerBoundary = newIndex +1;
                    tmp.m_UpperBoundary = rarityListItr->m_UpperBoundary - rarityListItr->m_LowerBoundary;
                    filteredList.push_back(*rarityListItr); 
                }
            }            
        }

        if ( !considerRarity )
        {
            for( unsigned int i = 1; i <= m_ItemRarityList.size(); ++i )
            {
                m_ItemRarityList[i].m_LowerBoundary = newIndex;
                m_ItemRarityList[i].m_UpperBoundary = newIndex;
                newIndex++;
            }
        }
        
        if ( filteredList.empty() )
            return nullptr;
        
        //Generate a random number in the length of the rarity index.
        const unsigned int randomNumber = SeededRandom::SeededRandomGenerator::GetRandomNumber(1,filteredList.back().m_UpperBoundary);

        std::string baseItemName, modifierName;
        for( rarityListItr = filteredList.begin(); rarityListItr != filteredList.end(); rarityListItr++ )
        {
            if (  rarityListItr->m_LowerBoundary <= randomNumber && randomNumber <= rarityListItr->m_UpperBoundary  ) 
            {
                baseItemName = rarityListItr->m_BaseItemName;
                modifierName = rarityListItr->m_ModifierName;
                break;
            }
        }

        if ( !baseItemName.empty() && !modifierName.empty() )
            return GenerateItem(baseItemName,modifierName); 
        
        return nullptr;  
    }

    ReikonCore::Item* ItemFactory::GenerateItem(const std::string& baseItemName,const std::string& modifierName)
    {
        //Iterate trough the item types
        for( Json::ValueIterator itemTypeItr = m_ItemsTemplate.begin() ; 
            itemTypeItr != m_ItemsTemplate.end() ; itemTypeItr++ )
        {
            //Iterate trough the base items in the specific item-type group
            for( Json::ValueIterator baseItemItr = m_ItemsTemplate[itemTypeItr.key().asString()]["BaseItem"].begin() ; 
                baseItemItr != m_ItemsTemplate[itemTypeItr.key().asString()]["BaseItem"].end() ; baseItemItr++ )
            {
                std::string localBaseItemName = baseItemItr.key().asString();
                int baseItemLevelRequirement = (*baseItemItr)["LevelRequirement"].asInt();
              
                if ( localBaseItemName !=  baseItemName)
                    continue;
                    
                //Go trough the modification group
                for( Json::ValueIterator itemModifGroupItr = m_ItemsTemplate[itemTypeItr.key().asString()]["ModifierGroups"].begin() ; 
                 itemModifGroupItr != m_ItemsTemplate[itemTypeItr.key().asString()]["ModifierGroups"].end() ; 
                 itemModifGroupItr++ )
                {
                    std::string modifierGroupLevel = itemModifGroupItr.key().asString();

                    //Go trough each modification in the given modification group. Example : "Layered" prefix modification.
                    for( Json::ValueIterator itemModifItr = m_ItemsTemplate[itemTypeItr.key().asString()]["ModifierGroups"][modifierGroupLevel].begin() ; 
                      itemModifItr != m_ItemsTemplate[itemTypeItr.key().asString()]["ModifierGroups"][modifierGroupLevel].end() ; 
                          itemModifItr++ )
                    {
                        std::string localModifierName = itemModifItr.key().asString();
                        if (  localModifierName != modifierName  )
                            continue;

                        std::string complexLocalizedNameID = "#LOC_" + modifierName + baseItemName;
                        ReikonCore::ItemSlotType localItemType = GetItemSlotTypeFromString( itemTypeItr.key().asString() ); //Get the Item slot type enum from str.
                        ReikonCore::Item* newItem = new ReikonCore::Item(complexLocalizedNameID,localItemType,baseItemLevelRequirement);
                        if ( !newItem )
                            return nullptr;
                        
                        //Go trough each modification type in modifications. Example dexterity-flat
                        for( Json::ValueIterator modifTypeItr = m_ItemsTemplate[itemTypeItr.key().asString()]["ModifierGroups"][modifierGroupLevel][localModifierName]["Modifications"].begin() ; 
                          modifTypeItr != m_ItemsTemplate[itemTypeItr.key().asString()]["ModifierGroups"][modifierGroupLevel][localModifierName]["Modifications"].end() ; 
                              modifTypeItr++ ) 
                        {
                            //Modification itself ( ex: Dexterity )
                            std::string modifierToAndType = modifTypeItr.key().asString();
                            std::string itemStatTarget = ItemStatTarget(modifierToAndType);
                            std::string itemStatType = ItemStatType(modifierToAndType);
                            float modificationValue = (*itemModifItr)["Modifications"][modifierToAndType].asFloat();
                            
                            if ( itemStatType == "Flat"  ) //By design items can only add Flat modifications to attributes. ( Code can support multipliers too. )
                            {
                                ReikonCore::ItemModifier newItemModifier(GetAttributeTypeFromString(itemStatTarget),false,modificationValue);
                                newItem->AddModification(newItemModifier);
                            }
                        }
                        
                        return newItem;
                    }
                }

                return nullptr; //We did not found the item. If the base item is found but no modifier for it we stop. 
            }
        }

        return nullptr;
    }
    
    ReikonCore::ItemSlotType ItemFactory::GetItemSlotTypeFromString(const std::string& baseGroupName) const
    {
        if ( baseGroupName == "Helmet" )
            return ReikonCore::ItemSlotType::Helmet;
        if ( baseGroupName == "ShoulderPlate" )
            return ReikonCore::ItemSlotType::ShoulderPlate;
        if ( baseGroupName == "Necklace" )
            return ReikonCore::ItemSlotType::Necklace;
        if ( baseGroupName == "Chest" )
            return ReikonCore::ItemSlotType::Chest;
        if ( baseGroupName == "Armband" )
            return ReikonCore::ItemSlotType::Armband;
        if ( baseGroupName == "Gloves" )
            return ReikonCore::ItemSlotType::Gloves;
        if ( baseGroupName == "Ring" )
            return ReikonCore::ItemSlotType::Ring1;
        if ( baseGroupName == "Belt" )
            return ReikonCore::ItemSlotType::Belt;
        if ( baseGroupName == "Boots" )
            return ReikonCore::ItemSlotType::Boots;
        if ( baseGroupName == "Weapon" )
            return ReikonCore::ItemSlotType::LeftHand;

        return ReikonCore::ItemSlotType::None;
    }

    ReikonCore::AttributeType ItemFactory::GetAttributeTypeFromString(const std::string& attributeName) const
    {
        if ( attributeName == "MaxHealth" )
            return ReikonCore::AttributeType::MaxHealth;
        if ( attributeName == "MaxMana" )
            return ReikonCore::AttributeType::MaxMana;
        if ( attributeName == "MaxStamina" )
            return ReikonCore::AttributeType::MaxStamina;
        if ( attributeName == "Strength" )
            return ReikonCore::AttributeType::Strength;
        if ( attributeName == "Intelligence" )
            return ReikonCore::AttributeType::Intelligence;
        if ( attributeName == "Dexterity" )
            return ReikonCore::AttributeType::Dexterity;
        if ( attributeName == "Wisdom" )
            return ReikonCore::AttributeType::Wisdom;
        if ( attributeName == "Charisma" )
            return ReikonCore::AttributeType::Charisma;

        return ReikonCore::AttributeType::None;
    }

    bool ItemFactory::CheckLevelRequirement(int baseItemLevel, const std::string& modifierGroup) const
    {
        //example : Group-*-1-10
        std::size_t found = modifierGroup.find("-*-");
        std::string corrected = modifierGroup.substr(found+3);
        //Now we only have 1-10
        int minlevel,maxlevel;
        found = corrected.find('-');
        minlevel = std::stoi(corrected.substr(0,found) ); //string to int
        maxlevel =  std::stoi(corrected.substr(found+1) ); 
    
        if( baseItemLevel >= minlevel && baseItemLevel <= maxlevel )
            return true;

        return false;
    }

    std::string ItemFactory::ItemStatType(const std::string& itemStat) const
    {
        std::size_t found = itemStat.find('-');
        std::string corrected = itemStat.substr(found+1);
        return corrected;
    }

    std::string ItemFactory::ItemStatTarget(const std::string& itemStat) const
    {
        std::size_t found = itemStat.find('-');
        std::string corrected = itemStat.substr(0,found);
        return corrected;
    }
}