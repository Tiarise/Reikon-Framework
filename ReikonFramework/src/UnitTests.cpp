﻿#include "Dependencies/CatchUnitTest/catch_amalgamated.hpp"
#include "Core/Reikon.h"
#include "Dependencies/SeededRandom.hpp"
#include <algorithm> //std::find
#include "Core/FrameworkTypes.h"
#include "Modues/ItemFactory.h"
#include "Dependencies/LocalizationManager/LocalizationManager.h"

TEST_CASE( "1) Seeded Random", "[SeededRandom]" )
{
    /*
        Test if the generator gives same number sequence with same seed.
        To repeat same sequence seed needs to be reset!
    */
    std::vector<unsigned int> randomListOne;
    std::vector<unsigned int> randomListTwo;
    
    SeededRandom::SeededRandomGenerator::SetRandomSeed(1);
    for( int i = 0 ; i < 10; ++i )
        randomListOne.push_back( SeededRandom::SeededRandomGenerator::GetRandomNumber(0,100) );

    SeededRandom::SeededRandomGenerator::SetRandomSeed(1);
    for( int i = 0 ; i < 10; ++i )
        randomListTwo.push_back( SeededRandom::SeededRandomGenerator::GetRandomNumber(0,100) );

    for( int i = 0 ; i < 10; ++i )
    {
        REQUIRE( randomListOne[i] == randomListTwo[i] );
    }
}

TEST_CASE( "2) Reikon Base Functionality", "[ReikonBaseFunctionality]" )
{
    ReikonCore::Reikon creatureOne("#LOC_CreatureName_LordFungusTheFirst", "#LOC_Faction_Human");

    //Each creature must have a unique ID
    REQUIRE( !creatureOne.UniqueID().empty() );
    
    //Localized name accessors.
    REQUIRE( creatureOne.LocalizedNameID() == "#LOC_CreatureName_LordFungusTheFirst" );
    creatureOne.LocalizedNameID("#LOC_CreatureName_LordFungusTheSecond");
    REQUIRE( creatureOne.LocalizedNameID() == "#LOC_CreatureName_LordFungusTheSecond" );

    //Localized faction accessors.
    REQUIRE( creatureOne.LocalizedFactionID() == "#LOC_Faction_Human" );
    creatureOne.LocalizedFactionID("#LOC_Faction_Dwarf");
    REQUIRE( creatureOne.LocalizedFactionID() == "#LOC_Faction_Dwarf" );

    //Invulnerability accessors.
    REQUIRE( creatureOne.Invulnerable() == false );
    creatureOne.Invulnerable(true);
    REQUIRE( creatureOne.Invulnerable() == true );
    
    //Two creatures must have a different ID!
    ReikonCore::Reikon creatureTwo("#LOC_CreatureName_LordDoughnutTheSecond", "#LOC_Faction_Dwarf");
    REQUIRE( creatureOne.UniqueID() != creatureTwo.UniqueID() );

    /*
        Move construction
        You may get a warning that creatureOne is used after it was moved,
        that's fine, that was the intention here. We check if the move was truly executed.
    */
    std::string creatureOneID = creatureOne.UniqueID();
    ReikonCore::Reikon creatureThree(std::move(creatureOne));
    REQUIRE( creatureThree.LocalizedNameID() == "#LOC_CreatureName_LordFungusTheSecond" );
    REQUIRE( creatureThree.LocalizedFactionID() == "#LOC_Faction_Dwarf" );
    REQUIRE( creatureThree.Invulnerable() == true );
    REQUIRE( creatureThree.UniqueID() == creatureOneID );
    REQUIRE( creatureOne.LocalizedNameID().empty() );
    REQUIRE( creatureOne.LocalizedFactionID().empty() );
    REQUIRE( creatureOne.Invulnerable() == false );
    REQUIRE( creatureOne.UniqueID().empty() );
    
    //AttributeHandler and Inventory must be present on demand!
    REQUIRE( creatureThree.AttributeHandler() != nullptr );
    REQUIRE( creatureThree.Inventory() != nullptr );
}

TEST_CASE( "3) Attribute Creation", "[AttributeCreation]" )
{
    ReikonCore::Reikon creatureOne("#LOC_CreatureName_LordFungusTheFirst", "#LOC_Faction_Human");
    
    //Create attribute Health for creatureOne. ( Option one - Create it in place )
    ReikonCore::Attribute* health = creatureOne.AttributeHandler()->AddNewAttribute("#LOC_Attribute_Health",
        ReikonCore::AttributeType::MaxHealth, ReikonCore::AttributeGroup::Vital, 100);

    //Create attribute Strength for creatureOne. ( Option two - Pass an existing attribute )
    ReikonCore::Attribute strengthLocal("#LOC_Attribute_Strength",ReikonCore::AttributeType::Strength,
        ReikonCore::AttributeGroup::MainAttribute, 10);
    ReikonCore::Attribute* strength = creatureOne.AttributeHandler()->AddNewAttribute(strengthLocal);
    
    //In the second case the strengthLocal was moved, check that.
    REQUIRE(strengthLocal.LocalizedNameID().empty());
    REQUIRE(strengthLocal.Type() == ReikonCore::AttributeType::None);
    REQUIRE(strengthLocal.Group() == ReikonCore::AttributeGroup::None);
    REQUIRE(strengthLocal.Value() == 0.0f);
    REQUIRE(strengthLocal.GetModifications().empty());

    //Check the two attributes if they are correctly present in the Handler with correct values.
    health = creatureOne.AttributeHandler()->GetAttribute(ReikonCore::AttributeType::MaxHealth);
    strength = creatureOne.AttributeHandler()->GetAttribute(ReikonCore::AttributeType::Strength);
    
    REQUIRE(health != nullptr);
    REQUIRE(health->LocalizedNameID() == "#LOC_Attribute_Health");
    REQUIRE(health->Type() == ReikonCore::AttributeType::MaxHealth);
    REQUIRE(health->Group() == ReikonCore::AttributeGroup::Vital);
    REQUIRE(health->Value() == 100.0f);
    REQUIRE(health->GetModifications().empty());

    REQUIRE(strength != nullptr);
    REQUIRE(strength->LocalizedNameID() == "#LOC_Attribute_Strength");
    REQUIRE(strength->Type() == ReikonCore::AttributeType::Strength);
    REQUIRE(strength->Group() == ReikonCore::AttributeGroup::MainAttribute);
    REQUIRE(strength->Value() == 10.0f);
    REQUIRE(strength->GetModifications().empty());
}

TEST_CASE( "4) Attribute Modifiers", "[AttributeModifiers]" )
{
    ReikonCore::Reikon creatureOne("#LOC_CreatureName_LordFungusTheFirst", "#LOC_Faction_Human");
    
    //Create attribute Health
    ReikonCore::Attribute* health = creatureOne.AttributeHandler()->AddNewAttribute("#LOC_Attribute_Health",
        ReikonCore::AttributeType::MaxHealth, ReikonCore::AttributeGroup::Vital, 100);
    //Create attribute Mana
    ReikonCore::Attribute* mana = creatureOne.AttributeHandler()->AddNewAttribute("#LOC_Attribute_Mana",
        ReikonCore::AttributeType::MaxMana, ReikonCore::AttributeGroup::Vital, 80);
    //Create attribute Strength
    ReikonCore::Attribute* strength = creatureOne.AttributeHandler()->AddNewAttribute("#LOC_Attribute_Strength",
        ReikonCore::AttributeType::Strength, ReikonCore::AttributeGroup::MainAttribute, 30);
    //Create attribute Intelligence
    ReikonCore::Attribute* intelligence = creatureOne.AttributeHandler()->AddNewAttribute("#LOC_Attribute_Intelligence",
        ReikonCore::AttributeType::Intelligence, ReikonCore::AttributeGroup::MainAttribute, 60);
    //Create attribute Dexterity
    ReikonCore::Attribute* dexterity = creatureOne.AttributeHandler()->AddNewAttribute("#LOC_Attribute_Dexterity",
        ReikonCore::AttributeType::Dexterity, ReikonCore::AttributeGroup::MainAttribute, 50);

    //Strength should modify health by 0.5 times of it's value.
    health->AddModification(strength,0.5f);
    REQUIRE(health->Value() == 115.0f);
    
    //Dexterity should modify health by 0.1 times of it's value.
    health->AddModification(dexterity,0.1f);
    REQUIRE(health->Value() == 120.0f);

    //Dexterity should modify strength by 0.5 times of it's value.
    strength->AddModification(dexterity,0.5f);
    REQUIRE(strength->Value() == 55.0f);
    REQUIRE(health->Value() == 132.5f); //100 + 27.5 + 5

    //Intelligence should modify Mana by 1.5 times of it's value.
    mana->AddModification(intelligence,1.5f);
    REQUIRE(mana->Value() == 170.0f);

    //Remove Strength to Health modification and check results
    health->RemoveModification(strength);
    REQUIRE(health->Value() == 105.0f); //Dexterity still modifies it!
    REQUIRE(mana->Value() == 170.0f);
    REQUIRE(strength->Value() == 55.0f);
    REQUIRE(intelligence->Value() == 60.0f);
    REQUIRE(dexterity->Value() == 50.0f);
    
    /*
        Try to create a circle in the modification chain.
        Any modifier attempting to do this should be ignored.
    */
    ReikonCore::Reikon creatureTwo("#LOC_CreatureName_LordFungusTheSecond", "#LOC_Faction_Dwarf");
    
    ReikonCore::Attribute* attribOne = creatureTwo.AttributeHandler()->AddNewAttribute("#LOC_Attribute_One",
        ReikonCore::AttributeType::MaxHealth, ReikonCore::AttributeGroup::Vital, 10);
    
    ReikonCore::Attribute* attribTwo = creatureTwo.AttributeHandler()->AddNewAttribute("#LOC_Attribute_Two",
        ReikonCore::AttributeType::MaxMana, ReikonCore::AttributeGroup::Vital, 20);
    
    ReikonCore::Attribute* attribThree = creatureTwo.AttributeHandler()->AddNewAttribute("#LOC_Attribute_Three",
        ReikonCore::AttributeType::Strength, ReikonCore::AttributeGroup::MainAttribute, 30);
    
    ReikonCore::Attribute* attribFour = creatureTwo.AttributeHandler()->AddNewAttribute("#LOC_Attribute_Four",
        ReikonCore::AttributeType::Intelligence, ReikonCore::AttributeGroup::MainAttribute, 40);
    
    ReikonCore::Attribute* attribFive = creatureTwo.AttributeHandler()->AddNewAttribute("#LOC_Attribute_Five",
        ReikonCore::AttributeType::Dexterity, ReikonCore::AttributeGroup::MainAttribute, 50);

    std::string modificationID = attribTwo->AddModification(attribOne,0.5f);
    REQUIRE(attribTwo->Value() == 25.0f);
    attribThree->AddModification(attribTwo,0.1f);
    REQUIRE(attribThree->Value() == 32.5f);
    attribFour->AddModification(attribThree,2.0f);
    REQUIRE(attribFour->Value() == 105.0f);
    attribFive->AddModification(attribFour,0.1f);
    REQUIRE(attribFive->Value() == 60.5f);

    /*
        This is where we try to connect five back to one.
        This request should be ignored and Attrib One should still be 10.
    */
    attribOne->AddModification(attribFour,0.1f);
    REQUIRE(attribOne->Value() == 10.0f);

    /*
        Remove the first modification
        We could use attribTwo that would remove all modification originating from attribTwo,
        but just for showcasing purposes we use the ID based remove.
    */
    attribTwo->RemoveModification(modificationID); //Could use attribTwo if needed.
    REQUIRE(attribTwo->Value() == 20.0f);
    REQUIRE(attribThree->Value() == 32.0f);
    REQUIRE(attribFour->Value() == 104.0f);
    REQUIRE(attribFive->Value() == 60.4f); //Float comparison on some compilers might be a problem...

    //Attribute handler Level checks
    REQUIRE(creatureTwo.AttributeHandler()->ReikonLevel() == 1);
    creatureTwo.AttributeHandler()->ReikonLevel(15);
    REQUIRE(creatureTwo.AttributeHandler()->ReikonLevel() == 15);
}

TEST_CASE( "5) Item Creation and Inventory test", "[Items]" )
{
    //Create a new item.
    ReikonCore::Item newItem("#LOC_Item_FancyNecklace",ReikonCore::ItemSlotType::Necklace,5,
        "#LOC_Item_FancyNecklace_Description","#LOC_Item_FancyNecklace_FlavorText");

    //Each item must have a unique ID
    REQUIRE( !newItem.UniqueID().empty() );

    //Function tests
    REQUIRE( newItem.Type() == ReikonCore::ItemSlotType::Necklace );
    REQUIRE( newItem.RequiredLevel() == 5 );
    newItem.RequiredLevel(25);
    REQUIRE( newItem.LocalizedNameID() == "#LOC_Item_FancyNecklace" );
    REQUIRE( newItem.LocalizedDescriptionID() == "#LOC_Item_FancyNecklace_Description" );
    REQUIRE( newItem.LocalizedFlavorTextID() == "#LOC_Item_FancyNecklace_FlavorText" );

    //Item modifier test
    REQUIRE( newItem.GetModifications().empty() );
    ReikonCore::ItemModifier itemModifier = ReikonCore::ItemModifier(ReikonCore::AttributeType::Intelligence,false,10);
    newItem.AddModification(itemModifier);
    REQUIRE( newItem.GetModifications().size() == 1 );

    //Create character, needed for Inventory and ItemModifier tests
    ReikonCore::Reikon creatureOne("#LOC_CreatureName_LordFungusTheFirst", "#LOC_Faction_Human");
    
    //Inventory size checks
    REQUIRE( creatureOne.Inventory()->BackpackSize() == 10 ); //Default size.
    REQUIRE( creatureOne.Inventory()->GetSlots().size() == 25 ); //Base equip slots + 10 backpack slot

    //Pick the necklace slot and check if default values are correct.
    REQUIRE( creatureOne.Inventory()->GetSlotByType(ReikonCore::ItemSlotType::Necklace)->SlotType() == ReikonCore::ItemSlotType::Necklace );
    REQUIRE( creatureOne.Inventory()->GetSlotByType(ReikonCore::ItemSlotType::Necklace)->SlotLock() == false );
    REQUIRE( creatureOne.Inventory()->GetSlotByType(ReikonCore::ItemSlotType::Necklace)->ItemInSlot() == nullptr );
    REQUIRE( !creatureOne.Inventory()->GetSlotByType(ReikonCore::ItemSlotType::Necklace)->UniqueID().empty() );

    //Add item to inventory
    ReikonCore::InventorySlot* backpackSlot = creatureOne.Inventory()->AddToInventory(newItem);
    REQUIRE( backpackSlot->SlotType() == ReikonCore::ItemSlotType::Backpack );
    REQUIRE( backpackSlot->ItemInSlot() == &newItem );
    
    //Item equip test
    ReikonCore::Attribute* intelligence = creatureOne.AttributeHandler()->AddNewAttribute("#LOC_Attribute_Intelligence",
    ReikonCore::AttributeType::Intelligence, ReikonCore::AttributeGroup::MainAttribute, 60);

    ReikonCore::InventorySlot* necklaceSlot = creatureOne.Inventory()->EquipItem(backpackSlot,ReikonCore::ItemSlotType::Necklace);
    REQUIRE( necklaceSlot->ItemInSlot() == &newItem );
    REQUIRE( backpackSlot->ItemInSlot() == nullptr );
    REQUIRE( intelligence->Value() == 70.0f); //Int was modified by the item.

    //Item unequip test
    backpackSlot = creatureOne.Inventory()->UnEquipItem(necklaceSlot);
    REQUIRE( necklaceSlot->ItemInSlot() == nullptr );
    REQUIRE( backpackSlot->ItemInSlot() == &newItem );
    REQUIRE( intelligence->Value() == 60.0f);
}

TEST_CASE( "6) Item Factory test", "[ItemFactory]" )
{
    /*
        For this to work put the ItemList.json next to the executable.
        [!] Pls note that if using this from Rider or VisualStudio, set the working directory correctly.
    */
    std::vector<ReikonCore::Item*> randomItems;
    ReikonModules::ItemFactory itemFactory;
    itemFactory.SetConfigFolder(""); //No folder, check in place.
    itemFactory.LoadItemData();
    itemFactory.BuildItemListWithRarity();
    for ( int i = 0; i < 20 ; ++i )
    {
        ReikonCore::Item* newItem = itemFactory.GenerateRandomItem(ReikonCore::ItemSlotType::Chest);
        if ( newItem )
            randomItems.emplace_back(newItem);
    }
    
    REQUIRE( randomItems.size() == 20 );
}

TEST_CASE( "7) Localization tests", "[Localization]" )
{
    //Create localization manager, this stores the localized texts paired with the #LOC IDs.
    Localization::LocalizationManager localizationManager;
    
    //Create item for localizations tests
    ReikonCore::Item newItem("#LOC_Item_FancyNecklace",ReikonCore::ItemSlotType::Necklace,5,
    "#LOC_Item_FancyNecklace_Description","#LOC_Item_FancyNecklace_FlavorText");
    
    localizationManager.SetText(newItem.LocalizedDescriptionID(),Localization::Language::English,"Blue necklace of intellect");
    localizationManager.SetText(newItem.LocalizedDescriptionID(),Localization::Language::Hungarian,"A tudás kék medálja");
    REQUIRE( localizationManager.GetText(newItem.LocalizedDescriptionID(),Localization::Language::English) == "Blue necklace of intellect" );
    REQUIRE( localizationManager.GetText(newItem.LocalizedDescriptionID(),Localization::Language::Hungarian) == "A tudás kék medálja" );
}