﻿#pragma once
#include "Item.h"
#include "FrameworkTypes.h"
#include "../Dependencies/CDelegate.h"

namespace ReikonCore
{
    struct InventorySlot
    {
        InventorySlot();
        InventorySlot(class Inventory* owningInventory, ItemSlotType slotType, Item* itemInSlot, bool slotLock);
        bool operator==(const InventorySlot& other) const;

        ItemSlotType SlotType() const;
        void SlotType(ItemSlotType slotType);

        Item* ItemInSlot() const;
        void ItemInSlot(Item* item);

        bool SlotLock() const;
        void SlotLock(bool locked);

        std::string UniqueID() const;
    
    private:
        void EmitSlotChanged();
        
        ItemSlotType m_SlotType;
        Item* m_ItemInSlot;
        bool m_SlotLock;
        std::string m_UniqueID;

        class Inventory* m_OwningInventory;
    };

    class Inventory
    {
    public:
        Inventory(class AttributeHandler* attributeHandler, unsigned int backpackSize = 10);
        virtual ~Inventory() = default;
        Inventory(const Inventory& orig) = delete;
        Inventory& operator=(const Inventory& orig) = delete;
        Inventory(Inventory&& orig) noexcept;
        Inventory& operator=(Inventory&& orig) noexcept;

        InventorySlot* GetSlotByItem(const Item& item);
        InventorySlot* GetSlotByType(ItemSlotType type);
        
        InventorySlot* AddToInventory(Item& item, bool equip = false);
        bool RemoveFromInventory(const InventorySlot* slot);

        InventorySlot* EquipItem(InventorySlot* slot, ItemSlotType targetSlot = ItemSlotType::None);
        InventorySlot* UnEquipItem(InventorySlot* slot);

        void BackpackSize(unsigned int size);
        unsigned int BackpackSize() const;
        
        const std::vector<InventorySlot>& GetSlots();
        
        //Inventory events
        CDelegate::CEvent<InventorySlot*> OnSlotChanged;
        
    private:
        void ToggleItemModifiers(Item* item, bool activate) const;
        
        unsigned int m_BackpackSize;
        std::vector<InventorySlot> m_Equipment;
        AttributeHandler* m_AttributeHandler;
    };
}
