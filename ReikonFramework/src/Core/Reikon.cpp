#include "Reikon.h"
#include "../Dependencies/UUIDGenerator.hpp"

namespace ReikonCore
{
	/*
		###################################################
		#            Special member functions             #
		###################################################
	 */
	Reikon::Reikon(std::string nameID, std::string factionID, const bool invulnerable) :
		m_LocalizedNameID(std::move(nameID)), m_LocalizedFactionID(std::move(factionID)), m_Invulnerable(invulnerable)
	{
		m_Id = sole::uuid4().str();
	}

	
	Reikon::Reikon(Reikon&& orig) noexcept
	{
		m_LocalizedNameID = std::move(orig.m_LocalizedNameID);
		m_LocalizedFactionID = std::move(orig.m_LocalizedFactionID);
		m_Invulnerable = orig.m_Invulnerable;
		m_Id = orig.m_Id;
		m_AttributeHandler = std::move(orig.m_AttributeHandler);
		m_Inventory = std::move(orig.m_Inventory);

		//Reset original object's non moved properties.
		orig.m_Invulnerable = false;
		orig.m_Id = "";
	}

	Reikon& Reikon::operator=(Reikon&& orig) noexcept
	{
		if ( this == &orig )
			return *this; //Should not move self to self.

		m_LocalizedNameID = std::move(orig.m_LocalizedNameID);
		m_LocalizedFactionID = std::move(orig.m_LocalizedFactionID);
		m_Invulnerable = orig.m_Invulnerable;
		m_Id = orig.m_Id;

		//Destroy currently managed objects. ( If we don't do this we will cause a memory leak. ) // TODO CHECK A MOVE LIKE THIS IF THE ATTRIB HANDLER WILL USE DESTRUCTOR? STILL GOOD PRACTISE IF NOT !!!!!!
		m_AttributeHandler.reset();
		m_Inventory.reset();
		
		m_AttributeHandler = std::move(orig.m_AttributeHandler);
		m_Inventory = std::move(orig.m_Inventory);

		//Reset original object's non moved properties.
		orig.m_Invulnerable = false;
		orig.m_Id = "";

		return *this;
	}

	/*
		###################################################
		#             Main Class Functions                #
		###################################################
	*/

	std::string Reikon::LocalizedNameID() const
	{
		return m_LocalizedNameID.LocalizationID();
	}

	void Reikon::LocalizedNameID(const std::string& name)
	{
		if ( name.empty() )
			return;

		m_LocalizedNameID.LocalizationID(name);
	}

	std::string Reikon::LocalizedFactionID() const
	{
		return m_LocalizedFactionID.LocalizationID();
	}

	void Reikon::LocalizedFactionID(const std::string& faction)
	{
		m_LocalizedFactionID.LocalizationID(faction);
	}

	std::string Reikon::UniqueID() const
	{
		if ( !m_Id.empty() )
			return m_Id;
		
		return "";
	}

	AttributeHandler* Reikon::AttributeHandler()
	{
		//Use make_unique, this way long type-names can be written once + make_unique was implemented with type safety checks.
		if ( m_AttributeHandler == nullptr )
			m_AttributeHandler = std::make_unique<ReikonCore::AttributeHandler>();

		return m_AttributeHandler.get();
	}

	Inventory* Reikon::Inventory()
	{
		if ( m_Inventory == nullptr )
			m_Inventory = std::make_unique<ReikonCore::Inventory>(AttributeHandler());

		return m_Inventory.get();
	}

	bool Reikon::Invulnerable() const
	{
		return m_Invulnerable;
	}

	void Reikon::Invulnerable(const bool& invulnerable)
	{
		m_Invulnerable = invulnerable;
	}
}