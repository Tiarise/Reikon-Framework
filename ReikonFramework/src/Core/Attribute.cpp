#include "Attribute.h"
#include "Item.h"
#include <algorithm>
#include "../Dependencies/LocalizationManager/LocalizationManager.h"

namespace ReikonCore
{
	/*
		###################################################
		#            Special member functions             #
		###################################################
	 */
	Attribute::Attribute(std::string localizedNameID, const AttributeType type, const AttributeGroup group, float baseValue)
	{
		m_LocalizedNameID = std::move(localizedNameID); //This calls the LocalizedText one param constructor.
		m_Type = type;
		m_Group = group;
		m_BaseValue = baseValue;
		m_ModificationsSum = 0.0f;
		m_ModificationsSumNonTemp = 0.0f;
	}

	Attribute::Attribute(Attribute&& orig) noexcept
	{
		m_LocalizedNameID = std::move(orig.m_LocalizedNameID);
		m_Type = orig.m_Type;
		m_Group = orig.m_Group;
		m_BaseValue = orig.m_BaseValue;
		m_ModificationsSum = orig.m_ModificationsSum;
		m_ModificationsSumNonTemp = orig.m_ModificationsSumNonTemp;
		m_IncomingModifications = std::move(orig.m_IncomingModifications);

		//Clean the orig object's non moved variables
		orig.m_Type = AttributeType::None;
		orig.m_Group = AttributeGroup::None;
		orig.m_BaseValue = 0.0f;
		orig.m_ModificationsSum = 0.0f;
		orig.m_ModificationsSumNonTemp = 0.0f;
	}

	Attribute& Attribute::operator=(Attribute&& orig) noexcept
	{
		if ( this == &orig )
			return *this; //Should not move self to self.

		m_LocalizedNameID = std::move(orig.m_LocalizedNameID);
		m_Type = orig.m_Type;
		m_Group = orig.m_Group;
		m_BaseValue = orig.m_BaseValue;
		m_ModificationsSum = orig.m_ModificationsSum;
		m_ModificationsSumNonTemp = orig.m_ModificationsSumNonTemp;
		m_IncomingModifications = std::move(orig.m_IncomingModifications);

		//Clean the orig object's non moved variables
		orig.m_Type = AttributeType::None;
		orig.m_Group = AttributeGroup::None;
		orig.m_BaseValue = 0.0f;
		orig.m_ModificationsSum = 0.0f;
		orig.m_ModificationsSumNonTemp = 0.0f;
		
		return *this;
	}

	/*
		###################################################
		#             Main Class Functions                #
		###################################################
	 */

	std::string Attribute::LocalizedNameID() const
	{
		return m_LocalizedNameID.LocalizationID();
	}

	void Attribute::LocalizedNameID(const std::string& localizedNameID)
	{
		m_LocalizedNameID.LocalizationID(localizedNameID);
	}

	AttributeType Attribute::Type() const
	{
		return m_Type;
	}

	AttributeGroup Attribute::Group() const
	{
		return m_Group;
	}

	float Attribute::Value(bool noTemp) const
	{
		if ( noTemp )
			return m_BaseValue + m_ModificationsSumNonTemp;

		return m_BaseValue + m_ModificationsSum;
	}

	float Attribute::BaseValue() const
	{
		return m_BaseValue;
	}

	bool Attribute::ChangeBaseValueBy(float value)
	{
		m_BaseValue += value;
		RecalculateModifications();
		return true;
	}

	std::string Attribute::AddModification(const T_VrtAI& source, const float modification, const bool multiplier, const bool temporary)
	{
		if ( !std::holds_alternative<Attribute*>(source) && !std::holds_alternative<Item*>(source) )
			return "";

		AttributeModification localModif(source, modification,multiplier,temporary);
		return AddModification(localModif);
	}

	std::string Attribute::AddModification(AttributeModification& modifier)
	{
		if ( !modifier.IsValid() )
			return "";

		/*
			One attrib can modify an another multiple times, but the same modifier ( by ID ) can't modify the
			an attribute multiple times.
		*/
		using IteratorType = std::vector<AttributeModification>::iterator;
		const IteratorType itr = std::find(m_IncomingModifications.begin(), m_IncomingModifications.end(), modifier);  //RANGES FIND? jobb a szintaxis akkor elfogadom
		if ( itr != m_IncomingModifications.end() )
			return "";

		//If source originates from an attribute we need to check if this new modifier would create a cycle in the modification chain.
		Attribute* sourceElement = nullptr;
		if ( std::holds_alternative<Attribute*>(modifier.m_Source) )
		{
			sourceElement = std::get<Attribute*>(modifier.m_Source);
			if ( sourceElement && !sourceElement->IsChainAcyclicTo(this) )
				return ""; //We would create a cyclic graph from modifications, that is not allowed.
		}

		//Add incoming modification. ( Somebody modifies this Attribute )
		AttributeModification& newModif = m_IncomingModifications.emplace_back(std::move(modifier));
		
		//If the modifier originates from an attribute we need to subscribe to the source to listen for changes. ( Items can't be modified while equipped so those are fine! )
		if ( sourceElement )
		{
			newModif.m_SourceModifiedEventHandle = sourceElement->Event_AttributeChanged.BindClassMemberDelegate<Attribute>(this, &Attribute::OnDependencyChanged);
			newModif.m_SourceDestroyedEventHandle = sourceElement->Event_AttributeDestroyed.BindClassMemberDelegate<Attribute>(this, &Attribute::OnDependencyDestroyed);
		}
		
		//Recalculate chain
		RecalculateModifications();
		return newModif.m_ID;
	}

	bool Attribute::RemoveModification(const T_VrtAI& source, bool recalculateTree, bool doNotUnsubscribe)
	{
		if ( !std::holds_alternative<Attribute*>(source) && !std::holds_alternative<Item*>(source) )
			return false;

		bool found = false;
		for ( unsigned int i = 0; i < m_IncomingModifications.size(); ++i )
		{
			if ( m_IncomingModifications[i].m_Source == source )
			{
				Attribute* sourceElement = std::get<Attribute*>(m_IncomingModifications[i].m_Source);
				if ( sourceElement && !doNotUnsubscribe )
				{
					sourceElement->Event_AttributeChanged.Remove(m_IncomingModifications[i].m_SourceModifiedEventHandle);
					sourceElement->Event_AttributeDestroyed.Remove(m_IncomingModifications[i].m_SourceDestroyedEventHandle);
				}
				
				m_IncomingModifications.erase(m_IncomingModifications.begin() + i);
				--i;
				found = true; //An if would be more costly than just setting this.
				if ( m_IncomingModifications.empty() )
					break;
			}
		}
		
		if ( found && recalculateTree )
			RecalculateModifications();
			
		return true;
	}
	
	bool Attribute::RemoveModification(const std::string& modifierID, bool recalculateTree)
	{
		if ( modifierID.empty() )
			return false;
		
		for ( unsigned int i = 0; i < m_IncomingModifications.size(); ++i )
		{
			if ( m_IncomingModifications[i].m_ID == modifierID )
			{
				if ( std::holds_alternative<Attribute*>(m_IncomingModifications[i].m_Source) )
				{
					Attribute* sourceElement = std::get<Attribute*>(m_IncomingModifications[i].m_Source);
					sourceElement->Event_AttributeChanged.Remove(m_IncomingModifications[i].m_SourceModifiedEventHandle);
					sourceElement->Event_AttributeDestroyed.Remove(m_IncomingModifications[i].m_SourceDestroyedEventHandle);
				}
				
				//Remove the incoming modification
				m_IncomingModifications.erase(m_IncomingModifications.begin() + i);
				if ( recalculateTree )
					RecalculateModifications();
				return true; //There can't be more modifiers with same ID!
			}
		}
		
		return false;		
	}
	
	const std::vector<AttributeModification>& Attribute::GetModifications() const
	{
		return m_IncomingModifications;
	}

	void Attribute::RecalculateModifications()
	{
		const float oldValue = Value();

		m_ModificationsSum = 0.0f;
		m_ModificationsSumNonTemp = 0.0f;

		for ( AttributeModification& modif : m_IncomingModifications )
		{
			/*
				Temp modification WONT be calculated into other attributes modifications!
				Say for example we have Strength at 10 with non temp modifications and at 20 with additional
				temp modifications. If say strength modifies Endurance with 0.5 value we only take 10*0.5 not with 20.
			*/
			if ( modif.m_Multiplier )
			{
				//Multiplier modifer is only usable with Attribute(m_Source) type.
				if ( !std::holds_alternative<Attribute*>(modif.m_Source) )
					continue;

				const Attribute* attrib = std::get<Attribute*>(modif.m_Source);
				if ( !attrib )
					continue;

				if ( !modif.m_Temporary )
					m_ModificationsSumNonTemp += attrib->Value(true) * modif.m_Modification;
				
				m_ModificationsSum += attrib->Value(true) * modif.m_Modification;
			}
			else
			{
				if ( !modif.m_Temporary )
					m_ModificationsSumNonTemp += modif.m_Modification;
				
				m_ModificationsSum += modif.m_Modification;
			}
		}

		//All attributes that this is modified by have to be recalculated!
		Event_AttributeChanged.Broadcast(this,oldValue,Value());
	}

	bool Attribute::IsChainAcyclicTo(const Attribute* attribToFind) const
	{
		/*
			* Checks the integrity of the modification chain.
			* This only checks for attribute modifications.
			* This is used to verify if the attribute graph is a directed acyclic graph.
		*/
		
		if ( attribToFind == this )
			return false; //Can't modify self, that would also create a circle. ( Also useless. )

		//Try to trace back the modification chain to the original element we tried to modify (attribToFind).
		for ( const AttributeModification& modifItr : GetModifications() )
		{
			if ( !std::holds_alternative<Attribute*>(modifItr.m_Source) )
				continue; //We are only interested in attribute originated modifiers.

			const Attribute* chainElement = std::get<Attribute*>(modifItr.m_Source);
			if ( !chainElement )
				continue;

			if ( chainElement == attribToFind )
				return false; //We got back to the new element on the modification chain! This means with this addition we would create a cyclic graph!

			if ( !chainElement->IsChainAcyclicTo(attribToFind) )
				return  false;
		}

		return true;
	}
	
	void Attribute::OnDependencyChanged(Attribute* attrib, float, float)
	{
		RecalculateModifications();
	}

	void Attribute::OnDependencyDestroyed(Attribute* attrib)
	{
		/*
			Do not unsubscribe here! That would make CEvent::Broadcast
			loose an element while iterating, cause a crash. We would remove
			an element here in RemoveModification from the CEvent.
			Later we might solve this in CEvent as commented in it.
			For now it's okay to leave out the unsubscribe here.
			If the source is destroyed it would be redundant anyway.
		*/
		RemoveModification(attrib,true,true);
	}
	
	/*
		###################################################
		#             AttributeModification               #
		###################################################
	*/

	AttributeModification::AttributeModification(const AttributeModification& orig) //Will make the two modifiers do the same thing! But the UUID(m_ID) will still be different on purpose!
	{
		m_ID = sole::uuid4().str(); 
		m_Source = orig.m_Source;
		m_Modification = orig.m_Modification;
		m_Multiplier = orig.m_Multiplier;
		m_Temporary = orig.m_Temporary;
		m_SourceModifiedEventHandle = orig.m_SourceModifiedEventHandle;
		m_SourceDestroyedEventHandle = orig.m_SourceDestroyedEventHandle;
	}
	
	AttributeModification& AttributeModification::operator=(const AttributeModification& orig) //This will make the two modifiers do the same thing! But the UUID(m_ID) will still be different on purpose!
	{
		if ( this == &orig)
			return *this; //Should not copy self to self.

		m_Source = orig.m_Source;
		m_Modification = orig.m_Modification;
		m_Multiplier = orig.m_Multiplier;
		m_Temporary = orig.m_Temporary;
		m_SourceModifiedEventHandle = orig.m_SourceModifiedEventHandle;
		m_SourceDestroyedEventHandle = orig.m_SourceDestroyedEventHandle;
		
		return *this;
	}

	AttributeModification::AttributeModification(AttributeModification&& orig) noexcept
	{
		m_ID = std::move(orig.m_ID); 
		m_Source = orig.m_Source;
		m_Modification = orig.m_Modification;
		m_Multiplier = orig.m_Multiplier;
		m_Temporary = orig.m_Temporary;
		m_SourceModifiedEventHandle = orig.m_SourceModifiedEventHandle;
		m_SourceDestroyedEventHandle = orig.m_SourceDestroyedEventHandle;

		//Clean the orig object's non moved variables
		orig.m_Source = T_VrtAI();
		orig.m_Modification = 1.0f;
		orig.m_Multiplier = true;
		orig.m_Temporary = false;
		orig.m_SourceModifiedEventHandle.Reset();
		orig.m_SourceDestroyedEventHandle.Reset();
	}
	
	AttributeModification& AttributeModification::operator=(AttributeModification&& orig) noexcept
	{
		if ( this == &orig)
			return *this; //Should not move self to self.

		m_ID = std::move(orig.m_ID); 
		m_Source = orig.m_Source;
		m_Modification = orig.m_Modification;
		m_Multiplier = orig.m_Multiplier;
		m_Temporary = orig.m_Temporary;
		m_SourceModifiedEventHandle = orig.m_SourceModifiedEventHandle;
		m_SourceDestroyedEventHandle = orig.m_SourceDestroyedEventHandle;

		//Clean the orig object's non moved variables
		orig.m_Source = T_VrtAI();
		orig.m_Modification = 1.0f;
		orig.m_Multiplier = true;
		orig.m_Temporary = false;
		orig.m_SourceModifiedEventHandle.Reset();
		orig.m_SourceDestroyedEventHandle.Reset();

		return *this;
	}

	bool AttributeModification::operator==(const AttributeModification& other) const
	{
		return m_ID == other.m_ID;
	}

	bool AttributeModification::IsValid() const
	{
		if ( !std::holds_alternative<Attribute*>(m_Source) && !std::holds_alternative<Item*>(m_Source) )
			return false;
		if ( m_Multiplier && !std::holds_alternative<Attribute*>(m_Source) )
			return false; //Only an attribute modifier can use the multiplier modifier.

		return true;
	}

}