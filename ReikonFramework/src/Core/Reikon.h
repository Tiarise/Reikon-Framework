#pragma once
#include <memory>
#include <string>
#include "AttributeHandler.h"
#include "Inventory.h"
#include "../Dependencies/LocalizationManager/LocalizationManager.h"

namespace ReikonCore
{
	class Reikon
	{
	public:
		Reikon(std::string nameID, std::string factionID, const bool invulnerable = false);

		virtual ~Reikon() = default;
		Reikon(const Reikon& orig) = delete;
		Reikon& operator=(const Reikon& orig) = delete;
		Reikon(Reikon&& orig) noexcept; //Copy is not supported but move is! ( There can't be two entities of the same ID! )
		Reikon& operator=(Reikon&& orig) noexcept;

		std::string LocalizedNameID() const;
		void LocalizedNameID(const std::string& name);

		std::string LocalizedFactionID() const;
		void LocalizedFactionID(const std::string& faction);

		std::string UniqueID() const;

		class AttributeHandler* AttributeHandler(); //Class is here for "changes meaning of" error.
		class Inventory* Inventory(); //Class is here for "changes meaning of" error.

		bool Invulnerable() const;
		void Invulnerable(const bool& invulnerable);

	private:
		Localization::LocalizedTextID m_LocalizedNameID;
		Localization::LocalizedTextID m_LocalizedFactionID;
		bool m_Invulnerable;
		std::string m_Id;

		std::unique_ptr<ReikonCore::AttributeHandler> m_AttributeHandler = nullptr;
		std::unique_ptr<ReikonCore::Inventory> m_Inventory = nullptr;
	};
}

