#pragma once
#include <memory>
#include <variant>

/*
	Shared types in one header.
	Put things here that has to be included into many things.
 */
namespace ReikonCore
{
	/*
		###################################################
		#                 Type aliases                    #
		###################################################
	*/
	using T_UptrA = std::unique_ptr<class Attribute>;
	using T_VrtAI = std::variant<class Attribute*, class Item*>;
	
	/*
		###################################################
		#                 Shared Types                    #
		###################################################
	*/
	enum class AttributeGroup
	{
		None,
		Vital,
		MainAttribute,
		SecondaryAttribute,
	};

	enum class AttributeType
	{
		None,
		MaxHealth,
		MaxMana,
		MaxStamina,
		Strength,
		Intelligence,
		Dexterity,
		Wisdom,
		Charisma
	};

	enum class ItemSlotType : unsigned int
	{
		None = 0,
		Helmet = 1,
		ShoulderPlate = 2,
		Necklace = 3,
		Chest = 4,
		Armband = 5,
		Gloves = 6,
		Ring1 = 7,
		Ring2 = 8,
		Ring3 = 9,
		Ring4 = 10,
		Belt = 11,
		Trousers = 12,
		Boots = 13,
		LeftHand = 14,
		RightHand = 15,
		Backpack = 16,
	};

	enum class ItemModifierGroup
	{
		None,
		Prefix,
		Suffix
	};
}