#pragma once
#include <string>
#include <variant>
#include <vector>
#include "FrameworkTypes.h"
#include "../Dependencies/CDelegate.h"
#include "../Dependencies/LocalizationManager/LocalizationManager.h"
#include "../Dependencies/UUIDGenerator.hpp"

namespace ReikonCore
{
	//Forward declaration.
	struct AttributeModification;

	/*
		This class handles the Attribute logic.
		An element for a system, handling character/object values that can
		be modified by multiple sources.
		
		Attributes can only modify Attributes.
		Items can only modify Attributes.

		Only the modified item knows of the modification, the source not. ( On purpose. )
	 */
	class Attribute
	{
	public:
		Attribute(std::string localizedNameID, const AttributeType type, const AttributeGroup group, float baseValue = 0);
		virtual ~Attribute() = default;
		Attribute(const Attribute& orig) = delete;
		Attribute& operator=(const Attribute& orig) = delete;
		Attribute(Attribute&& orig) noexcept;
		Attribute& operator=(Attribute&& orig) noexcept;

		std::string LocalizedNameID() const;
		void LocalizedNameID(const std::string& localizedNameID);

		AttributeType Type() const;
		AttributeGroup Group() const;
		float Value(bool noTemp = false) const;

		float BaseValue() const;
		bool ChangeBaseValueBy(float value);

		std::string AddModification(const T_VrtAI& source, const float modification, const bool multiplier = true, const bool temporary = false);
		std::string AddModification(AttributeModification& modifier);
		bool RemoveModification(const T_VrtAI& source, bool recalculateTree = true, bool doNotUnsubscribe = false);
		bool RemoveModification(const std::string& modifierID, bool recalculateTree = true);
		const std::vector<AttributeModification>& GetModifications() const;
		void RecalculateModifications();

		bool IsChainAcyclicTo(const Attribute* attribToFind) const;

		CDelegate::CEvent<Attribute*, float, float> Event_AttributeChanged; //Pre,Post value. ( With temp modifications. )
		CDelegate::CEvent<Attribute*> Event_AttributeDestroyed;
		
	private:
		void OnDependencyChanged(Attribute* attrib, float, float);
		void OnDependencyDestroyed(Attribute* attrib);
		
		Localization::LocalizedTextID m_LocalizedNameID;
		AttributeType m_Type;
		AttributeGroup m_Group;
		float m_BaseValue;
		float m_ModificationsSum; //Contains temporary modifications too!
		float m_ModificationsSumNonTemp;
		std::vector<AttributeModification> m_IncomingModifications;
	};
	
	/*
		This struct describes the modification received by the attribute.
		* m_Source: From where the modification originates.
		* m_Modification: This is the modifier value from the source.
		* m_Multiplier: This means that the target attribute got a multiplier as a modification from the source.
		  In this case the m_Source only can be an Attribute. Items don't support this. Items don't have a base
		  value that can be taken as a multiplier base.
		* m_Temporary: Means that this modification won't be calculated into other attribute modifiers and this is
		  a special temp modification. ( Used for short buffs or special effects. )
	
		Temp modification WONT be calculated into other attributes modifications!
		( See: RecalculateIncomingModifications )
	 */
	struct AttributeModification
	{
		AttributeModification() : m_ID(sole::uuid4().str()), m_Source(T_VrtAI()), m_Modification(1.0f),
		m_Multiplier(true), m_Temporary(false) { }
		
		AttributeModification(const T_VrtAI source, const float modification, const bool multiplier = true, const bool temporary = false)
			: m_ID(sole::uuid4().str()), m_Source(source), m_Modification(modification), m_Multiplier(multiplier), m_Temporary(temporary) {}

		virtual ~AttributeModification() = default;
		AttributeModification(const AttributeModification& orig); 
		AttributeModification& operator=(const AttributeModification& orig);
		AttributeModification(AttributeModification&& orig) noexcept;
		AttributeModification& operator=(AttributeModification&& orig) noexcept;
		
		bool operator==(const AttributeModification& other) const;
		bool IsValid() const;
		
		std::string m_ID;
		T_VrtAI m_Source; //See AttributeHandler.h description why this is raw.
		float m_Modification;
		bool m_Multiplier;
		bool m_Temporary;
		
		CDelegate::EventHandle m_SourceModifiedEventHandle;
		CDelegate::EventHandle m_SourceDestroyedEventHandle;
	};
}