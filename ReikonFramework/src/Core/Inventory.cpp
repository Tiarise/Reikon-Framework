#include "Inventory.h"
#include "AttributeHandler.h"
#include "../Dependencies/UUIDGenerator.hpp"

namespace ReikonCore
{
	/*
		###################################################
		#                 InventorySlot                   #
		###################################################
	*/

	InventorySlot::InventorySlot() : InventorySlot(nullptr,ItemSlotType::Backpack, nullptr, true) {} //Constructor delegation.

	InventorySlot::InventorySlot(class Inventory* owningInventory, ItemSlotType slotType, Item* itemInSlot, bool slotLock)
	{
		m_OwningInventory = owningInventory;
		m_SlotType = slotType;
		m_ItemInSlot = itemInSlot;
		m_SlotLock = slotLock;
		m_UniqueID = sole::uuid4().str();
	}

	bool InventorySlot::operator==(const InventorySlot& other) const
	{
		return m_UniqueID == other.m_UniqueID;
	}

	ItemSlotType InventorySlot::SlotType() const
	{
		return m_SlotType;
	}
	
	void InventorySlot::SlotType(ItemSlotType slotType)
	{
		m_SlotType = slotType;
		EmitSlotChanged();
	}
	
	Item* InventorySlot::ItemInSlot() const
	{
		return m_ItemInSlot;
	}
	
	void InventorySlot::ItemInSlot(Item* item)
	{
		m_ItemInSlot = item;
		EmitSlotChanged();
	}

	bool InventorySlot::SlotLock() const
	{
		return m_SlotLock;
	}
	
	void InventorySlot::SlotLock(bool locked)
	{
		m_SlotLock = locked;
		EmitSlotChanged();
	}

	std::string InventorySlot::UniqueID() const
	{
		return m_UniqueID;
	}

	void InventorySlot::EmitSlotChanged()
	{
		if ( m_OwningInventory )
			m_OwningInventory->OnSlotChanged.Broadcast(this);
	}

	/*
		###################################################
		#                  Inventory                      #
		###################################################
	*/

	Inventory::Inventory(AttributeHandler* attributeHandler, unsigned int backpackSize)
	{
		//Fill equipment slots ( From 1 so we ignore None and less that Backpack so we ignore that too. )
		for ( unsigned int i = 1; i < static_cast<unsigned int>(ItemSlotType::Backpack); ++i )
		{
			InventorySlot newSlot(this,static_cast<ItemSlotType>(i), nullptr, false);
			m_Equipment.emplace_back(newSlot);
		}

		//Fill backpack slots
		for ( unsigned int i = 0; i < backpackSize; i++ )
		{
			InventorySlot newSlot(this,ItemSlotType::Backpack,nullptr,false);
			m_Equipment.emplace_back(newSlot);
		}

		m_BackpackSize = backpackSize;
		m_AttributeHandler = attributeHandler;
	}

	Inventory::Inventory(Inventory&& orig) noexcept
	{
		m_BackpackSize = orig.m_BackpackSize;
		orig.m_BackpackSize = 0;
		m_Equipment = std::move(orig.m_Equipment);
		m_AttributeHandler = orig.m_AttributeHandler;
		orig.m_AttributeHandler = nullptr;
	}

	Inventory& Inventory::operator=(Inventory&& orig) noexcept
	{
		if ( this == &orig )
			return *this; //Should not move self to self.

		m_BackpackSize = orig.m_BackpackSize;
		orig.m_BackpackSize = 0;
		m_Equipment = std::move(orig.m_Equipment);
		m_AttributeHandler = orig.m_AttributeHandler;
		orig.m_AttributeHandler = nullptr;
		
		return *this;
	}
	
	InventorySlot* Inventory::GetSlotByItem(const Item& item)
	{
		for ( InventorySlot& slot : m_Equipment )
		{
			if ( slot.ItemInSlot() && slot.ItemInSlot()->UniqueID() == item.UniqueID() )
				return &slot;
		}
		
		return  nullptr;
	}

	InventorySlot* Inventory::GetSlotByType(ItemSlotType type)
	{
		if ( type == ItemSlotType::None )
			return nullptr;

		/*
			If the slot we are searching is a Backpack slot, only return with an empty one.
			Otherwise return with the slot. There is no sense in searching for an occupied Backpack slot.
			If looking for a given item use GetSlotByItem or GetSlots if there is no ptr.
		*/
		for ( InventorySlot& slot : m_Equipment )
		{
			if ( slot.SlotType() == type )
			{
				if ( slot.SlotType() == ItemSlotType::Backpack )
				{
					if ( slot.ItemInSlot() == nullptr )
						return &slot;
				}
				else
					return &slot; 
			}
		}

		return nullptr;
	}
	

	InventorySlot* Inventory::AddToInventory(Item& item, bool equip)
	{
		if ( GetSlotByItem(item) )
			return nullptr; //Already in the inventory.
		
		InventorySlot* Slot = GetSlotByType(ItemSlotType::Backpack);
		if ( !Slot )
			return nullptr; //No appropriate slot found.
		if ( Slot->SlotLock() || Slot->ItemInSlot() )
			return nullptr; //If locked or there is an item in place return. ( We do not replace, use UnEquipItem )

		Slot->ItemInSlot(&item); 
		return Slot;
	}

	bool Inventory::RemoveFromInventory(const InventorySlot* slot)
	{
		for ( unsigned int i = 0; i < m_Equipment.size(); ++i )
		{
			InventorySlot& slotItr = m_Equipment[i];
			if ( slot->UniqueID() != slotItr.UniqueID() )
				continue;

			m_Equipment.erase(std::next(m_Equipment.begin(),i));
			return true;
		}

		return false;
	}

	InventorySlot* Inventory::EquipItem(InventorySlot* slot, ItemSlotType targetSlot)
	{
		if ( targetSlot == ItemSlotType::None || targetSlot == ItemSlotType::Backpack )
			return nullptr; //Target must be a valid equip slot.
		if ( slot->SlotType() != ItemSlotType::Backpack )
			return nullptr; //We can only equip things from the backpack.
		if ( !slot->ItemInSlot() || slot->ItemInSlot()->Type() == ItemSlotType::Backpack )
			return nullptr; //If the item is intended for backpack use only, then ignore request.

		//The the item's type equip slot.
		InventorySlot* equipSlot = GetSlotByType(slot->ItemInSlot()->Type());
		if ( !equipSlot || equipSlot->ItemInSlot() )
			return nullptr; //Slot not found or occupied.

		//Change item in slots.
		equipSlot->ItemInSlot(slot->ItemInSlot());
		slot->ItemInSlot(nullptr);

		//Set the item equipped
		equipSlot->ItemInSlot()->SetEquipSlot(equipSlot);

		//Activate item modifiers
		ToggleItemModifiers(equipSlot->ItemInSlot(),true);
		
		return equipSlot;
	}

	InventorySlot* Inventory::UnEquipItem(InventorySlot* slot)
	{
		if ( !slot->ItemInSlot() || slot->SlotType() == ItemSlotType::Backpack )
			return nullptr; //There is no item in slot or item is not equipped.

		//Try to get a free backpack slot.
		InventorySlot* backpackSlot = GetSlotByType(ItemSlotType::Backpack);
		if ( !backpackSlot )
			return nullptr;
		
		//Change item in slots.
		backpackSlot->ItemInSlot(slot->ItemInSlot());
		slot->ItemInSlot(nullptr);

		//Deactivate item modifiers
		ToggleItemModifiers(backpackSlot->ItemInSlot(),false);
		
		return backpackSlot;
	}

	void Inventory::BackpackSize(unsigned int size)
	{
		m_BackpackSize = size;
	}

	unsigned int Inventory::BackpackSize() const
	{
		return m_BackpackSize;
	}
	
	const std::vector<InventorySlot>& Inventory::GetSlots()
	{
		return m_Equipment;
	}

	void Inventory::ToggleItemModifiers(Item* item, bool activate) const
	{
		if ( !item )
			return;

		//Here we handle the item modifier activation/deactivation here
		for ( ItemModifier& modif : item->GetModifications() )
		{
			if ( modif.TargetAttributeType != AttributeType::None )
			{
				Attribute* attrib =  m_AttributeHandler->GetAttribute(modif.TargetAttributeType);
				if ( !attrib )
					return;

				if ( activate )
				{
					//Create modifier ( Item attribute modifiers are never temporary! )
					AttributeModification newModif(item,modif.Value,modif.Multiplier);

					//Save the UUID so we can remove if if needed. 
					modif.AppliedAttributeModifierID = newModif.m_ID;
					
					//Add modifier to the attribute
					attrib->AddModification(newModif);
				}
				else
				{
					//Add modifier to the attribute
					attrib->RemoveModification(modif.AppliedAttributeModifierID);

					//Remove the UUID
					modif.AppliedAttributeModifierID = "";
				}
			}
		}
	}
}