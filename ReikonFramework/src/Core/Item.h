#pragma once
#include <string>
#include <vector>
#include "FrameworkTypes.h"
#include "../Dependencies/LocalizationManager/LocalizationManager.h"

namespace ReikonCore
{
	/*
		Type that contains Item modifier data.
		It's extendable, for example we can have effect part too if it would
		add an effect. It should be component based not derived.
		Item::AddModification can't move with base so derived data would not be
		copied.
	 */
	struct ItemModifier
	{
		ItemModifier();
		ItemModifier(AttributeType attributeType, bool multiplier, float value);
		virtual ~ItemModifier() = default;
		ItemModifier(const Item& orig) = delete;
		ItemModifier& operator=(const ItemModifier& orig) = delete;
		ItemModifier(ItemModifier&& orig) noexcept;
		ItemModifier& operator=(ItemModifier&& orig) noexcept;
		
		bool operator==(const ItemModifier& other) const;
		virtual bool IsValid() const;

		//Core Item modifier data
		std::string ID;
		ItemModifierGroup Group;

		//Attribute modifier part data
		AttributeType TargetAttributeType;
		bool Multiplier;
		float Value;
		std::string AppliedAttributeModifierID; //The ID of the created modifier. ( Attribute part. )
	};
	
	class Item
	{
	public:
		Item(std::string localizedNameID, ItemSlotType type, int requiredLevel = 1);
		Item(std::string localizedNameID, ItemSlotType type, int requiredLevel, 
			std::string localizedDescriptionID, std::string localizedFlavorTextID);

		virtual ~Item() = default;
		Item(const Item& orig) = delete;
		Item& operator=(const Item& orig) = delete;
		Item(Item&& orig) noexcept;
		Item& operator=(Item&& orig) noexcept;
		bool operator==(const Item& other) const;

		std::string UniqueID() const;
		ItemSlotType Type() const;

		int RequiredLevel() const;
		void RequiredLevel(int requiredLevel);

		std::vector<ItemModifier>& GetModifications();
		std::string AddModification(ItemModifier& modifier); //Non const so move can happen. ( Copy Ctor is deleted. )
		void RemoveModification(const std::string& modifierID);

		void SetEquipSlot(const struct InventorySlot* slot); //Set by the inventory.

		std::string LocalizedNameID() const;
		void LocalizedNameID(std::string localizedNameID);
		std::string LocalizedDescriptionID() const;
		void LocalizedDescriptionID(std::string localizedDescriptionID);
		std::string LocalizedFlavorTextID() const;
		void LocalizedFlavorTextID(std::string localizedFlavorTextID);

	private:
		std::string m_UniqueID;
		Localization::LocalizedTextID m_LocalizedNameID;
		Localization::LocalizedTextID m_LocalizedDescriptionID;
		Localization::LocalizedTextID m_LocalizedFlavorTextID;
		ItemSlotType m_Type;
		int m_RequiredLevel;
		std::vector<ItemModifier> m_Modifications;
		const struct InventorySlot* m_EquipSlot;
	};
}

