#include "Item.h"
#include <algorithm>
#include "../Dependencies/UUIDGenerator.hpp"
#include "../Dependencies/LocalizationManager/LocalizationManager.h"

namespace ReikonCore
{
	/*
		###################################################
		#               ItemModifier types                #
		###################################################
	*/
	ItemModifier::ItemModifier()
	{
		ID = sole::uuid4().str();
		Group = ItemModifierGroup::None;
	}

	ItemModifier::ItemModifier(AttributeType attributeType, bool multiplier, float value) : ItemModifier()
	{
		TargetAttributeType = attributeType;
		Multiplier = multiplier;
		Value = value;
	}

	ItemModifier::ItemModifier(ItemModifier&& orig) noexcept
	{
		ID = std::move(orig.ID);
		Group = orig.Group;
		TargetAttributeType = orig.TargetAttributeType;
		Multiplier = orig.Multiplier;
		Value = orig.Value;

		//Reset non-moved values
		orig.Group = ItemModifierGroup::None;
		orig.TargetAttributeType = AttributeType::None;
		orig.Multiplier = false;
		orig.Value = 0.0f;
	}
	
	ItemModifier& ItemModifier::operator=(ItemModifier&& orig) noexcept
	{
		if ( this == &orig )
			return *this; //Should not move self to self.
		
		ID = std::move(orig.ID);
		Group = orig.Group;

		//Reset non-moved values
		orig.Group = ItemModifierGroup::None;
		
		return *this;
	}

	bool ItemModifier::operator==(const ItemModifier& other) const
	{
		return ID == other.ID;
	}
	
	bool ItemModifier::IsValid() const
	{
		return !ID.empty();
	}

	/*
		###################################################
		#                     Item                        #
		###################################################
	*/

	Item::Item(std::string localizedNameID, ItemSlotType type, int requiredLevel)
	{
		m_UniqueID = sole::uuid4().str();
		m_LocalizedNameID = std::move(localizedNameID);
		m_Type = type;
		m_RequiredLevel = requiredLevel;
		m_EquipSlot = nullptr;
	}

	Item::Item(std::string localizedNameID, ItemSlotType type, int requiredLevel, std::string localizedDescriptionID, std::string localizedFlavorTextID)
		: Item(std::move(localizedNameID),type,requiredLevel)
	{
		m_LocalizedDescriptionID = std::move(localizedDescriptionID);
		m_LocalizedFlavorTextID = std::move(localizedFlavorTextID);
	}

	Item::Item(Item&& orig) noexcept
	{
		m_UniqueID = std::move(orig.m_UniqueID);
		m_LocalizedNameID = std::move(orig.m_LocalizedNameID);
		m_LocalizedDescriptionID = std::move(orig.m_LocalizedDescriptionID);
		m_LocalizedFlavorTextID = std::move(orig.m_LocalizedFlavorTextID);
		m_Type = orig.m_Type;
		m_RequiredLevel = orig.m_RequiredLevel;
		m_Modifications = std::move(orig.m_Modifications);
		m_EquipSlot = orig.m_EquipSlot;

		//Clean the orig object's non moved variables
		orig.m_Type = ItemSlotType::None;
		orig.m_RequiredLevel = 1;
		orig.m_EquipSlot = nullptr;
	}

	Item& Item::operator=(Item&& orig) noexcept
	{
		if ( this == &orig )
			return *this; //Should not move self to self.

		m_UniqueID = std::move(orig.m_UniqueID);
		m_LocalizedNameID = std::move(orig.m_LocalizedNameID);
		m_LocalizedDescriptionID = std::move(orig.m_LocalizedDescriptionID);
		m_LocalizedFlavorTextID = std::move(orig.m_LocalizedFlavorTextID);
		m_Type = orig.m_Type;
		m_RequiredLevel = orig.m_RequiredLevel;
		m_Modifications = std::move(orig.m_Modifications);
		m_EquipSlot = orig.m_EquipSlot;

		//Clean the orig object's non moved variables
		orig.m_Type = ItemSlotType::None;
		orig.m_RequiredLevel = 1;
		orig.m_EquipSlot = nullptr;
		
		return *this;
	}

	bool Item::operator==(const Item& other) const
	{
		return m_UniqueID == other.m_UniqueID;
	}

	std::string Item::UniqueID() const
	{
		return m_UniqueID;
	}

	ItemSlotType Item::Type() const
	{
		return m_Type;
	}

	int Item::RequiredLevel() const
	{
		return m_RequiredLevel;
	}

	void Item::RequiredLevel(int requiredLevel)
	{
		m_RequiredLevel = (std::max)(requiredLevel, 1);
	}

	std::vector<ItemModifier>& Item::GetModifications()
	{
		return m_Modifications;
	}

	std::string Item::AddModification(ItemModifier& modifier)
	{
		if ( !modifier.IsValid() || m_EquipSlot ) //Not allowed while item is equipped.
			return "";
		
		using IteratorType = std::vector<ItemModifier>::iterator;
		const IteratorType itr = std::find(m_Modifications.begin(), m_Modifications.end(), modifier);
		if ( itr != m_Modifications.end() )
			return ""; //Can't add the same modifier multiple times to the same item.

		const ItemModifier& movedModif = m_Modifications.emplace_back(std::move(modifier));
		return movedModif.ID;
	}

	void Item::RemoveModification(const std::string& modifierID)
	{
		if ( modifierID.empty() || m_EquipSlot ) //Not allowed while item is equipped.
			return;

		using IteratorType = std::vector<ItemModifier>::iterator;
		const IteratorType itr = std::find_if(m_Modifications.begin(), m_Modifications.end(),
			[modifierID](const ItemModifier& element){ return element.ID == modifierID; } );
		
		if ( itr == m_Modifications.end() )
			return;

		m_Modifications.erase(itr);
	}

	void Item::SetEquipSlot(const InventorySlot* slot)
	{
		if ( !slot )
			return;
			
		m_EquipSlot = slot;
	}

	std::string Item::LocalizedNameID() const
	{
		return m_LocalizedNameID.LocalizationID();
	}

	void Item::LocalizedNameID(std::string localizedNameID)
	{
		m_LocalizedNameID = std::move(localizedNameID);
	}

	std::string Item::LocalizedDescriptionID() const
	{
		return m_LocalizedDescriptionID.LocalizationID();
	}

	void Item::LocalizedDescriptionID(std::string localizedDescriptionID)
	{
		m_LocalizedDescriptionID = std::move(localizedDescriptionID);
	}

	std::string Item::LocalizedFlavorTextID() const
	{
		return m_LocalizedFlavorTextID.LocalizationID();
	}

	void Item::LocalizedFlavorTextID(std::string localizedFlavorTextID)
	{
		m_LocalizedFlavorTextID = std::move(localizedFlavorTextID);
	}
}