#pragma once
#include <iostream>

namespace ReikonCore
{
	/*
		###################################################
		#          Global Defines and Variables           #
		###################################################
	*/
	#define DEBUGENSURE

	enum class Language;
	inline Language DefaultLang = (Language)0; //Init to English. ( This way we don't have to include the definition. )
}
