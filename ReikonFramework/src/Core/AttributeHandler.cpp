#include "AttributeHandler.h"
#include <algorithm>

namespace ReikonCore
{
	AttributeHandler::~AttributeHandler()
	{
		for ( T_UptrA& element :  m_Attributes )
		{
			element->Event_AttributeDestroyed.Broadcast(element.get());
		}
	}
	
	AttributeHandler::AttributeHandler(AttributeHandler&& orig) noexcept
	{
		m_ReikonLevel = orig.m_ReikonLevel;
		orig.m_ReikonLevel = 1; //Reset.
		m_Attributes = std::move(orig.m_Attributes);
		OnAnyAttributeChanged = std::move(orig.OnAnyAttributeChanged);
	}

	AttributeHandler& AttributeHandler::operator=(AttributeHandler&& orig) noexcept
	{
		if ( this == &orig )
			return *this; //Should not move self to self.

		m_ReikonLevel = orig.m_ReikonLevel;
		orig.m_ReikonLevel = 1; //Reset.
		m_Attributes.clear(); //Destroy original objects.
		m_Attributes = std::move(orig.m_Attributes);
		OnAnyAttributeChanged = std::move(orig.OnAnyAttributeChanged);
		return *this;
	}

	int AttributeHandler::ReikonLevel() const
	{
		return m_ReikonLevel;
	}

	void AttributeHandler::ReikonLevel(int level)
	{
		//Added () around std::max due to <windows.h> min,max macros. This nullifies the macro application.
		m_ReikonLevel = (std::max)(level, 1); 
	}

	Attribute* AttributeHandler::GetAttribute(AttributeType type) const
	{
		for ( const T_UptrA& element : m_Attributes )
		{
			if ( !element )
				continue;
			
			if ( element->Type() == type )
				return element.get();
		}

		return nullptr;
	}

	Attribute* AttributeHandler::AddNewAttribute(std::string localizedNameID, const AttributeType type, const AttributeGroup group, const float baseValue)
	{
		if ( type == AttributeType::None )
			return nullptr;
			
		for ( const T_UptrA& element : m_Attributes )
		{
			if ( element->Type() == type )
				return nullptr;
		}
		
		const T_UptrA& newAttribute = m_Attributes.emplace_back(std::make_unique<Attribute>(localizedNameID,type,group,baseValue));
		newAttribute->Event_AttributeChanged.BindClassMemberDelegate<AttributeHandler>(this, &AttributeHandler::OnAttributeChangedCallback);
		return newAttribute.get();
	}

	Attribute* AttributeHandler::AddNewAttribute(Attribute& attribute)
	{
		if ( attribute.Type() == AttributeType::None )
			return nullptr;
		
		for ( const T_UptrA& element : m_Attributes )
		{
			if ( element->Type() == attribute.Type() )
				return nullptr;
		}

		const T_UptrA& newAttribute = m_Attributes.emplace_back(std::make_unique<Attribute>(std::move(attribute)));
		newAttribute->Event_AttributeChanged.BindClassMemberDelegate<AttributeHandler>(this, &AttributeHandler::OnAttributeChangedCallback);
		return newAttribute.get();
	}

	void AttributeHandler::OnAttributeChangedCallback(Attribute* attribute, float preValue, float postValue)
	{
		OnAnyAttributeChanged.Broadcast(attribute, preValue, postValue);
	}
}