#pragma once
#include <vector>
#include "Attribute.h"
#include "FrameworkTypes.h"

namespace ReikonCore
{
	/*
		The AttributeHandler is responsible for holding and managing
		the Attributes. From each AttributeType the AttributeHandler can
		only hold one! That's it's unique ID.

		I've considered raw/shared/unique pointers and references as candidates for storing Attributes.
		Here is my verdict:

		Raw Pointers
		------------------------------------------------------------------------------
		It's unsafe 'cos if the AttributeHandler won't call the destructor
		manually on AttributeHandler destruction or when removing the attribute from the list.
		Also you have to make sure that when we are not creating the Attribute in place but we get a ref
		as a param we have to create a new attrib here on the heap and copy data
		so we know the object won't get destroyed 'cos it went out of scope somewhere.
		Since C++11 we have smart pointers that can solve most of these issues.
			
		Shared Pointer
		------------------------------------------------------------------------------
		This would be good 'cos we could express that the AttributeHandler has shared ownership of the Attribute
		with affecting AttributeModifications and ItemToAttributeModifiers. Meaning the Attribute can't be destroyed
		until any of the three types of objects holds it. However this complicates the code quite a bit and the
		std::enable_shared_from_this would be needed so we won't create multiple shared objects with separate ref
		counts. But the shared pointer constructor doesn't considers the enable_shared_from_this central ref count and
		still creates a new shared pointer ( Even in C++20 ). And std::make_shared creates a new pointed object so
		we can't use that either. Maybe later if the upper mentioned issue will get solved I will consider it, like this
		I think is quite error prone from a coder standpoint. ( bad_weak ptr error )
			
		Unique Pointer
		------------------------------------------------------------------------------
		It expresses that the AttributeHandler has exclusive ownership of the attribute once
		it's inside the m_Attributes array. And will be automatically destroyed on AttributeHandler destruction or
		array element removal. Yes this means that the AttributeModifications/ItemToAttributeModifiers can only hold
		raw pointers, 'cos if the AttributeHandler would have unique and the other two would have shared pointers to
		the attribute, it would be destroyed once the upper two object would go out of scope. Smart pointers don't know
		about each other in C++. Unlike in C# how class refs work or in the UnrealReflection system how UPROPERTY
		pointers work. This is the best solution for us, 'cos References are almost good but have one flaw for us.
		That if we add more modifiers to the handler, the vector positions could move, thus the pointers to the
		references in the modifiers point to the wrong object on the stack. Since the unique ptr makes the data on the
		heap, we won't have this problem.

		Holding References
		------------------------------------------------------------------------------
		Now how the Unique Pointer version is different than this? Holding references is cheaper but also this means
		that the Attributes will be on the stack not on the heap and this causes the issue mentioned in the UniquePtr
		section.
	 */
	class AttributeHandler
	{
	public:
		AttributeHandler() = default;
		virtual ~AttributeHandler();
		AttributeHandler(const AttributeHandler& orig) = delete; 
		AttributeHandler& operator=(const AttributeHandler& orig) = delete;
		AttributeHandler(AttributeHandler&& orig) noexcept;
		AttributeHandler& operator=(AttributeHandler&& orig) noexcept;

		int ReikonLevel() const;
		void ReikonLevel(int level);
		
		Attribute* GetAttribute(AttributeType type) const;
		Attribute* AddNewAttribute(std::string localizedNameID, const AttributeType type, const AttributeGroup group, const float baseValue = 0);
		Attribute* AddNewAttribute(Attribute& attribute);
		
		CDelegate::CEvent<Attribute*,float,float> OnAnyAttributeChanged; //Pre,Post value.

	private:
		void OnAttributeChangedCallback(Attribute* attribute, float preValue, float postValue);

		int m_ReikonLevel = 1;
		std::vector<T_UptrA> m_Attributes;
	};
}
