#include "LocalizationManager.h"

namespace Localization
{
	/*
		###################################################
		#                 Static functions                #
		###################################################
	*/
	void ValidifyID(std::string& ID)
	{
		std::string prefix = ID.substr(0, 5);
		if ( prefix != "#LOC_" )
			ID = "#LOC_" + ID;
	}
	
	/*
		###################################################
		#                 LocalizedText                   #
		###################################################
	*/

	LocalizedTextID::LocalizedTextID(std::string localizationID)
	{
		LocalizationID(std::move(localizationID));
	}

	void LocalizedTextID::LocalizationID(std::string localizationID)
	{
		if ( localizationID.empty() )
			return;

		ValidifyID(localizationID);
		m_LocalizationID = std::move(localizationID);
	}

	std::string LocalizedTextID::LocalizationID() const
	{
		return  m_LocalizationID;
	}

	/*
		###################################################
		#              LocalizationManager                #
		###################################################
	*/

	void LocalizationManager::SetText(std::string id, Language lang, const std::string& text)
	{
		for ( TextHolder& element : m_Texts )
		{
			if ( element.ID != id )
				continue;
				
			for ( std::pair<Language, std::string>& localizedText : element.Data )
			{
				if ( localizedText.first == lang )
				{
					localizedText.second = text; //We override the existing text.
					return;
				}
			}

			//Put a new language entry with the new text.
			element.Data.emplace_back(lang, text);
		}

		//Make sure the ID has valid format.
		ValidifyID(id);

		//If we did not find the ID, that means this is a completely new data.
		m_Texts.emplace_back(id,lang,text);
	}

	std::optional<std::string> LocalizationManager::GetText(const std::string id, Language lang) const
	{
		for ( const TextHolder& element : m_Texts )
		{
			if ( element.ID != id )
				continue;

			for ( const std::pair<Language, std::string>& localizedText : element.Data )
			{
				if ( localizedText.first == lang )
					return localizedText.second;
			}
		}

		return nullptr;
	}
}