#pragma once

#include <string>
#include <vector>
#include <optional>

namespace Localization
{
	enum class Language
	{
		English,
		Hungarian,
	};

	static void ValidifyID(std::string& ID);
	
	/*
		This was made into a struct for better extensibility.
		If this were a simple string we would not be able to
		do a central formatting check. Also this way it's centrally
		extensible.
	 */
	struct LocalizedTextID
	{
		LocalizedTextID() = default;
		LocalizedTextID(std::string localizationID);
		~LocalizedTextID() = default;
		LocalizedTextID(const LocalizedTextID& orig) = default;
		LocalizedTextID& operator=(const LocalizedTextID& orig) = default;
		LocalizedTextID(LocalizedTextID&& orig) = default;
		LocalizedTextID& operator=(LocalizedTextID&& orig) = default;
		
		void LocalizationID(std::string localizationID);
		std::string LocalizationID() const;

	private:
		std::string m_LocalizationID;
	};

	class LocalizationManager
	{
	public:
		LocalizationManager() = default;
		~LocalizationManager() = default;
		LocalizationManager(const LocalizationManager& orig) = delete;
		LocalizationManager& operator=(const LocalizationManager& orig) = delete;
		LocalizationManager(LocalizationManager&& orig) = delete;
		LocalizationManager& operator=(LocalizationManager&& orig) = delete;
		
		void SetText(std::string id, Language lang, const std::string& text);
		std::optional<std::string> GetText(std::string id, Language lang) const;

	private:
		struct TextHolder
		{
			TextHolder() = default;

			TextHolder(std::string id, Language lang, std::string text) :
				ID(std::move(id)) { Data.emplace_back(lang,text); }

			std::string ID;
			std::vector <std::pair<Language, std::string>> Data;
		};

		std::vector<TextHolder> m_Texts; //TODO: Optimize this structure.
	};
}
