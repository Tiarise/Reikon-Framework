﻿#pragma once
#include <cstdlib>

namespace SeededRandom
{
    class SeededRandomGenerator
    {
    public:
        static void SetRandomSeed(unsigned int seed) { srand(seed); }
        
        unsigned static int GetRandomNumber(unsigned int min = 0,unsigned int max = RAND_MAX)
        {
            return rand() % max + min;
        }
    };
}