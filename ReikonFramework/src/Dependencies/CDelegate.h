#pragma once
#include <list>

/*
	Description:
	------------
	This is a custom one header delegate/event system made from scratch.
	
	Notes:
	------
	Since I've worked with UnrealEngine4 I've always adored their implementation of delegates in C++.
	Especially their "multicast" delegates. Those could store pointers to multiple type of functions
	and when the delegate is called all functions would be called. ( This is very very similar to C# events. )
	This technology is extremely useful for keeping the code integrity on healthy levels.
	You can encapsulate code that is meant to be together and keep the implementation hidden from the outside.
	Also this helps to keep the code decoupled for better maintainability. As I'm a big fan of Linux I'm also a big
	advocate of the mindset: "Do one thing, and do it well". This helps to do just that.

	Usage:
	------
	char GlobalTestFunction(float a) { return a; }
	...
	CStaticDelegate<char, float> StaticDelegate1(&GlobalTestFunction);
	char returnVal2 = StaticDelegate1.Execute('v');
	...
	TestClass test1; // Has func that returns a float and has in int param.
	CDelegate<TestClass,float, int> Delegate1(&test1,&TestClass::TestFunction);
	float returnVal = Delegate1.Execute(24);
	...
	CEvent<int,std::string> myEvent; //Can broadcast to functions with 2 params int,string and void return.
	EventHandle myHandle2 = myEvent.BindClassMemberDelegate<TestClass2>(&test2, &TestClass2::TestFunction5);
	EventHandle myHandle3 = myEvent.BindStaticDelegate(&TestFunction6);
	myEvent.Broadcast(42,"Test");

	CEvent<> myEvent2; //Event without argument
 */

 /*
	 #########################################################
	 #                      Delegate                         #
	 #                                                       #
	 #            Can reference one(!) method.               #
	 #             Return value can be given.                #
	 #########################################################
  */

namespace CDelegate
{
	/*
		Needed, so the EventData destructor can correctly clean up memory and destroy the
		polymorphic object. The virtual destructor is the key here!
	 */
	class IDelegate
	{
	public:
		IDelegate() = default;
		virtual ~IDelegate() noexcept = default;
	};

	/*
		Needed, so the EventData can use this as a base for calling
		Execute on the different types of delegates.
		The "void* m_DelegatePtr" is cast to this.
	 */
	template <typename ReturnVal, typename... Args>
	class CDelegateBase : IDelegate
	{
	public:
		virtual ReturnVal Execute(Args... args) = 0;
	};

	/*
		For static or global functions that have no reference to an owning object.
		CopyConstruct, CopyAssign, MoveConstruct, MoveAssign are handwritten 'cos
		some compilers doesn't support some unique functions = defaults.
		( VS2013 compiler doesn't support defaulted move constructors for example. )
	*/
	template <typename ReturnVal, typename... Args>
	class CStaticDelegate : public CDelegateBase<ReturnVal, Args...>
	{
	public:
		using DelegateFunctionPtr = ReturnVal(*)(Args...); //C++11 typedef

		CStaticDelegate() : m_Function(nullptr) {}
		CStaticDelegate(const DelegateFunctionPtr function) { Bind(function); }
		~CStaticDelegate() override { Release(); }

		CStaticDelegate(const CStaticDelegate& orig) { m_Function = orig.m_Function; }
		CStaticDelegate& operator=(const CStaticDelegate& orig) { m_Function = orig.m_Function; return *this; }
		CStaticDelegate(CStaticDelegate&& orig) noexcept { m_Function = orig.m_Function; orig.m_Function = nullptr; }
		CStaticDelegate& operator=(CStaticDelegate&& orig) noexcept { m_Function = orig.m_Function; orig.m_Function = nullptr; return *this; }

		void Bind(DelegateFunctionPtr function) { m_Function = function; }
		void Release() { m_Function = nullptr; }
		bool IsBound() const { return  m_Function != nullptr; }

		ReturnVal Execute(Args... args) override { return (*m_Function)(args...); }

	private:
		DelegateFunctionPtr m_Function;
	};

	/*
		For member functions of objects.
		CopyConstruct, CopyAssign, MoveConstruct, MoveAssign are handwritten 'cos
		some compilers doesn't support some unique functions = defaults.
		( VS2013 compiler doesn't support defaulted move constructors for example. )
	*/
	template <typename ObjectType, typename ReturnVal, typename... Args>
	class CDelegate : public CDelegateBase<ReturnVal, Args...>
	{
	public:
		using DelegateFunctionPtr = ReturnVal(ObjectType::*)(Args...); //C++11 typedef

		CDelegate() : m_Object(nullptr), m_Function(nullptr) {}
		CDelegate(ObjectType* object, const DelegateFunctionPtr function) { Bind(object, function); }
		~CDelegate() override { Release(); }

		CDelegate(const CDelegate& orig) { m_Object = orig.m_Object; m_Function = orig.m_Function; }
		CDelegate& operator=(const CDelegate& orig) { m_Object = orig.m_Object; m_Function = orig.m_Function; return *this; }
		CDelegate(CDelegate&& orig) noexcept { m_Object = orig.m_Object; orig.m_Object = nullptr; m_Function = orig.m_Function; orig.m_Function = nullptr; }
		CDelegate& operator=(CDelegate&& orig) noexcept { m_Object = orig.m_Object; orig.m_Object = nullptr; m_Function = orig.m_Function; orig.m_Function = nullptr; return *this; }

		void Bind(ObjectType* object, DelegateFunctionPtr function) { m_Object = object; m_Function = function; }
		void Release() { m_Object = nullptr; m_Function = nullptr; }
		bool IsBound() const { return  m_Function != nullptr; }
		ObjectType* GetOwnerObject() const { return m_Object; }

		ReturnVal Execute(Args... args) override { return (m_Object->*m_Function)(args...); }

	private:
		ObjectType* m_Object;
		DelegateFunctionPtr m_Function;
	};

	/*
		#########################################################
		#                        Event                          #
		#                                                       #
		#  Can reference multiple methods with same arguments,  #
		#       however the return value must be void!          #
		#                                                       #
		#########################################################
	 */


	 /*
		 An ID that can identify the delegate.
		 Every handle is unique!
		 0 means it's uninitialized. ( EventData is not bound! )
	 */
	class EventHandle
	{
	public:
		EventHandle() : m_Id(0) {}
		void Initialize() { m_Id = (ID_COUNTER == UINT_MAX - 1) ? 1 : ++ID_COUNTER; }
		bool IsInitialized() const { return m_Id != 0; }
		void Reset() { m_Id = 0; }
		bool operator==(EventHandle const& orig) const { return m_Id == orig.m_Id; }

	private:
		unsigned int m_Id;
		inline static unsigned int ID_COUNTER = 0; //Since C++17 static members can be initialized in header with inline.
	};

	/*
		This is the object that stores a void* to the delegate
		we created and registered our function call in.
	 */
	class EventData
	{
	public:
		template <typename... FunctionArgs>
		using DelegateStaticFunctionPtr = void(*)(FunctionArgs...);
		template <typename ObjectType, typename... FunctionArgs>
		using DelegateClassMemberFunctionPtr = void(ObjectType::*)(FunctionArgs...);

		template <typename... FunctionArgs>
		using MyDelegate = CDelegateBase<void, FunctionArgs...>;

		EventData() : m_DelegatePtr(nullptr) {}
		~EventData() { Release(); }

		EventData(const EventData& orig) = delete; //Copy or CopyAssign is not allowed!
		EventData& operator=(const EventData& orig) = delete;
		EventData(EventData&& orig) noexcept { m_DelegatePtr = orig.m_DelegatePtr = m_DelegatePtr = nullptr; }
		EventData& operator=(EventData&& orig) noexcept { Release(); m_DelegatePtr = orig.m_DelegatePtr = m_DelegatePtr = nullptr; return *this; }

		template <typename... FunctionArgs>
		EventHandle CreateStaticFunctionDelegate(DelegateStaticFunctionPtr<FunctionArgs...> function)
		{
			m_DelegatePtr = new CStaticDelegate<void, FunctionArgs...>(function);
			m_Handle.Initialize();
			return m_Handle;
		}

		template <typename ObjectType, typename... FunctionArgs>
		EventHandle CreateClassMemberDelegate(ObjectType* object, DelegateClassMemberFunctionPtr<ObjectType, FunctionArgs...> function)
		{
			m_DelegatePtr = new CDelegate<ObjectType, void, FunctionArgs...>(object, function);
			m_Handle.Initialize();
			return m_Handle;
		}

		void Release()
		{
			if ( !IsBound() )
				return;

			delete static_cast<IDelegate*>(m_DelegatePtr); //Destructor will be called. ( Static cast preserves the address. )
			m_DelegatePtr = nullptr;
		}

		EventHandle GetHandle() const { return m_Handle; }
		bool IsBound() const { return  m_DelegatePtr != nullptr; }

		template <typename... FunctionArgs>
		void Execute(FunctionArgs... args) { static_cast<MyDelegate<FunctionArgs...>*>(m_DelegatePtr)->Execute(args...); }

	private:
		void* m_DelegatePtr;
		EventHandle m_Handle;
	};

	/*
		The event itself.
		We require function ptr-s that return void and all
		functions param list must match, so when broadcasting we
		can give correct params.
		We use list as the Data container since that is is way more optimal here than a vector.
		We don't need random access to the data but when adding a new one it's cheaper 'cos no reallocation
		is needed when we would exceed the vector size.

		Vector Resize:
		4 steps: Create new storage, Move/copy new elements to the new storage, destroy old elements, free old storage.

		Todo maybe later: When using Broadcast, the CEvent should lock itself for Bind and remove.
		                  Or make a buffer that takes request then execute it at broadcast end.
		                  This way, the m_Events won't be changed during the for cycle.
	*/
	template <typename... FunctionArgs>
	class CEvent
	{
	public:
		using DelegateStaticFunctionPtr = void(*)(FunctionArgs...);
		template<typename ObjectType>
		using DelegateClassMemberFunctionPtr = void(ObjectType::*)(FunctionArgs...);

		bool IsBound(const EventHandle& handle) const
		{
			for ( const EventData& data : m_Events )
			{
				if ( data.GetHandle() == handle )
					return true;
			}

			return false;
		}

		void Remove(const EventHandle& handle)
		{
			for ( std::list<EventData>::iterator itr = m_Events.begin(); itr != m_Events.end(); ++itr )
			{
				if ( itr->GetHandle() == handle )
				{
					m_Events.erase(itr);
					break;
				}
			}
		}

		void RemoveAll() { m_Events.clear(); }

		template<typename ObjectType>
		EventHandle BindClassMemberDelegate(ObjectType* object, DelegateClassMemberFunctionPtr<ObjectType> function)
		{
			m_Events.emplace_back(EventData());
			return m_Events.back().CreateClassMemberDelegate<ObjectType, FunctionArgs...>(object, function);
		}

		EventHandle BindStaticDelegate(DelegateStaticFunctionPtr function)
		{
			m_Events.emplace_back(EventData());
			return m_Events.back().CreateStaticFunctionDelegate<FunctionArgs...>(function);
		}

		void Broadcast(FunctionArgs... args)
		{
			for ( EventData& data : m_Events )
				data.Execute<FunctionArgs...>(args...);
		}

	private:
		std::list<EventData> m_Events;
	};
}