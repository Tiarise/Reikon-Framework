#Helper script for cleaning up the Reikon project folder on windows
#If the script can't be executed -> Run addmin power shell then -> Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser

$DebugFolder = "../Debug"
$IdeaFolder = "../.idea"
$VScodeFolder = "../.vscode"
$BinariesFolder64Solution = "../x64"
$BinariesFolder32Solution = "../x86"
$BinariesFolder64Project = "../ReikonFramework/x64"
$BinariesFolder32Project = "../ReikonFramework/x86"
$VcxprojFilters = "../ReikonFramework/ReikonFramework.vcxproj.filters"
$DotSettingsUser = "../ReikonFramework.sln.DotSettings.user"


if (Test-Path $DebugFolder) { Remove-Item $DebugFolder -Recurse}
if (Test-Path $IdeaFolder) { Remove-Item $IdeaFolder -Recurse }
if (Test-Path $VScodeFolder) { Remove-Item $VScodeFolder -Recurse }
if (Test-Path $BinariesFolder64Solution) { Remove-Item $BinariesFolder64Solution -Recurse }
if (Test-Path $BinariesFolder32Solution) { Remove-Item $BinariesFolder32Solution -Recurse }
if (Test-Path $BinariesFolder64Project) { Remove-Item $BinariesFolder64Project -Recurse }
if (Test-Path $BinariesFolder32Project) { Remove-Item $BinariesFolder32Project -Recurse }
if (Test-Path $VcxprojFilters) { Remove-Item $VcxprojFilters }
if (Test-Path $DotSettingsUser) { Remove-Item $DotSettingsUser }

Write-Host 'Reikon Solution cleanup complete!'