#!/bin/sh
#This script helps you to comile and clean up the project folder with CMAKE.


CompileProject()
{
    cd ../
    cmake -G "Unix Makefiles" -S "." -B "."
    make
}

CleanProject()
{
    cd ../
    rm -rf build
    rm -rf CMakeFiles
    rm -rf .vscode
    rm cmake_install.cmake
    rm CMakeCache.txt
    rm Makefile
    echo "Project folder cleanup complete!"
}

ShowHelp()
{
    echo "Usage:"
    echo "Parameters: compile or clean."
}



if [ "$1" != "" ]; then

    if [ "$1" == "compile" ]; then
        CompileProject
    elif [ "$1" == "clean" ]; then
        CleanProject
    elif [ "$1" == "help" ]; then
        ShowHelp
    else
        echo "Wrong parameters. See: help"
    fi
else
    echo "No parameters. See: help"
fi
