# Reikon-Framework 3.0

This is a cross-platform C++ framework with attribute and inventory handling for characters.<br>
Originally created as a fun side project at the university. Later became part of my thesis.<br>
Since then, it has become a test ground for my non-game-engine related C++ coding adventures to
which I come back from time to time.

## Notable Features
- Basic character creation ( Contains the AttributeHandler and the Inventory. )
- Attribute management with modifiers chaining through them
- Item creation with attribute modifiers ( While equipped, modifying the attribute chain. )
- Inventory handling ( Backpack, Equipment etc... )
- Random item generation ( Using a template written in Json. ItemFactory uses it. )
- Custom hand written Event system. ( Inspired from UnrealEngine. )
- Unit testing ( Using Catch2. )

## Compilation
- Opening the solution in supported software. ( Rider, VisualStudio )<br>
- With the CMake system. ( Helper script provided in DevOpsScrips. - Linux only )

## Notes
- As of 3.0, this is a C++20 project.
- Folder clean up scripts provided in DevOpsScrips ( Windows, Linux )
- When opening the solution please use Build/Rebuild before first use.
- The framework-unit-tests need the ItemList.json to be next to the executable.

## Changelog

- ### 3.0 ( 2023-04-10 )
    - Update to C++20.
    - Introduction of move semantics to all applicable places.
    - Complete code refactor, removed unnecessary derived classes, moved logic to base.
    - Added CDelegate, a hand written event system.
    - Added LocalizationManager, hand written very basic support for localization.
    - Switched from GTest to Catch2 for unit-testing
    - Removed Skill, Faction related code. ( It's better to have in an Unreal project. )
    - Project structure reorganization

- ### 2.1 ( 2017-12-10 )
    - Update to C++17
    - Added SeededRandom, custom solution for safe randomization.
    - Descriptions, Flavor texts to items.

- ### 2.0 ( 2017-11-26 )
    - Update to modern C++. ( Using UniquePtr etc... )
    - 1.x versions were developed outside of source control and dating back to 2011.
